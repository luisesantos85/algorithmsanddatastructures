package hackerrank.com.datastructures;

public class Tree {

    public static void printTree(TreeNode node){
        if(node != null){
            printTree(node.left);
            printTree(node.right);
            System.out.print(node.data + " ");
        }
    }

    public static void main(String[] args){
        TreeNode node3 = new TreeNode(3);
        TreeNode node5 = new TreeNode(5);
        TreeNode node1 = new TreeNode(1);
        TreeNode node4 = new TreeNode(4);
        TreeNode node2 = new TreeNode(2);
        TreeNode node6 = new TreeNode(6);

        node3.left = node5;
        node3.right = node2;

        node5.left = node1;
        node5.right = node4;

        node2.left = node6;

        printTree(node3);
    }
}
