package hackerrank.com.datastructures;


public class LinkedList {

    /**
     * Creates a Linked List
     * */
    public static Node createLinkedList() {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
       // node5.next = node3;  // adding a cycle
        return node1;
    }

    /**
     * Prints a Linked List
     * */
    public static void printLinkedList(Node node){
        if(node != null){
            System.out.println(node.data);
            printLinkedList(node.next);
        }
    }

    /**
     * Inserts a node in the specified position of the Linked List Recursive
     * */
    public static Node insertNodeHead(Node head, int data){
        Node newNode = new Node(data);
        if(head != null){
            insertNodeHead(head.next, head.data);
            newNode.next = head;
        }
        return newNode;
    }


    /**
     * Inserts a node in the specified position of the Linked List
     * */
    public static Node insertNodeSpecificPositionLoop(Node head, int data, int position) {
        int i = 0;
        Node aux;
        Node node = new Node(data);

        if (head == null)
            return node;

        aux = head;

        if (position == 0) {
            node.next = head;
            return node;
        } else {
            for (i = 0; i < position; i++) {
                if (i == position - 1) {
                    Node next = aux.next;
                    aux.next = node;
                    node.next = next;
                } else {
                    aux = aux.next;
                }
            }
            return head;
        }
    }


    /**
     * Inserts a node in the specified position of the Linked List Recursive
     * */
    public static Node insertNodeSpecificPositionRecursive(Node node, int data, int position){
        Node n;
        Node aux;

        if(position == 0){
            n = new Node(data);
            n.next = node;
            return n;
        }else {
            if(index == position - 1) {
                n = new Node(data);
                aux = node.next;
                n.next = aux;
                node.next = n;
                return node;
            }else{
                if(node.next != null){
                    index++;
                    insertNodeSpecificPositionRecursive(node.next, data, position);
                }else{
                    n = new Node(data);
                    node.next = n;
                }
                return node;
            }
        }
    }

    /**
     * Deletes a Node using a loop
     * */
    public static Node deleteNodeLoop(Node head, int position){
        int i = 0;
        Node aux;

        aux = head;
        if(position == 0){
            aux = head.next;
            head.next = null;
            return aux;
        }else{
            for (i=0;i<position;i++){
                if(i == position-1){
                    if(aux.next != null)
                        aux.next = aux.next.next;
                    break;
                }else{
                    aux = aux.next;
                }
            }
            return head;
        }
    }

    public static void printLinkedListInReverse(Node node){
        if(node == null)
            return;

        if(node.next != null)
            printLinkedListInReverse(node.next);

        System.out.println(node.data);
    }

    public static Node reverseLinkedListLoop(Node node){
        Node previous = null;
        Node next;

        while(node != null){
            next = node.next;
            node.next = previous;

            previous = node;
            node = next;
        }

        return previous;
    }

    public static boolean cycleDetection(Node node){
        if(node == null)
            return false;

        Node fast = node.next;
        Node slow = node;

        boolean isCycle = false;

        while(fast != null && fast.next != null){
            if(fast == slow){
                isCycle = true;
                break;
            }
            slow = slow.next;
            fast = fast.next.next;
        }

        return isCycle;
    }

    public static int index;

    public static void main(String[] args){
        index = 0;
        Node head = createLinkedList();
        System.out.println(cycleDetection(head));

        //head = insertNodeSpecificPositionRecursive(head, 8, 1);

        //printLinkedList(head);
    }
}
