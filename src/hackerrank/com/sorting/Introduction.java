package hackerrank.com.sorting;

import java.io.File;
import java.util.Scanner;

public class Introduction {
	public static void main(String[] args) {
		Introduction.readInput();
    }
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			//Scanner input = new Scanner(System.in);
			
			String v = input.nextLine();
			int n = Integer.parseInt(input.nextLine());
			
			String[] array = input.nextLine().split(" ");
			
			for(int i=0;i<n;i++){
				if(array[i].equals(v)){
					System.out.println(i);
					break;
				}
			}
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
