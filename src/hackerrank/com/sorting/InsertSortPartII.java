package hackerrank.com.sorting;

import java.io.File;
import java.util.Scanner;

public class InsertSortPartII {
	
	 	public static void insertionSortPart2(int[] ar){       
	           // Fill up the code for the required logic here
	           // Manipulate the array as required
	           // The code for Input/Output is already provided
		 
		  int temp;
	        for (int i = 1; i < ar.length; i++) {
	            for(int j = i ; j > 0 ; j--){
	                if(ar[j] < ar[j-1]){
	                    temp = ar[j];
	                    ar[j] = ar[j-1];
	                    ar[j-1] = temp;
	                }
	            }
	            printArray(ar);
	        }
	    }  
	    
	      
	    public static void main(String[] args) {
	        try{
	        	//Scanner in = new Scanner(System.in);
		        
		    	File file = new File("/Users/luis/dev/input.txt");
	       		Scanner in = new Scanner(file);
	       		
		        int s = in.nextInt();
		        int[] ar = new int[s];
		        for(int i=0;i<s;i++){
		        	ar[i]=in.nextInt(); 
		        }
		        insertionSortPart2(ar);
		        
		        in.close();
	        }catch(Exception e){
	        	e.printStackTrace();
	        }               
	    }   
	    
	    private static void printArray(int[] ar) {
	      for(int n: ar){
	         System.out.print(n+" ");
	      }
	        System.out.println("");
	   }

}
