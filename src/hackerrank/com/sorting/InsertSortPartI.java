package hackerrank.com.sorting;

import java.io.File;
import java.util.Scanner;

public class InsertSortPartI {

	
	
    public static void main(String[] args) {
    	try{
    		//Scanner in = new Scanner(System.in);
    		File file = new File("/Users/luis/dev/input.txt");
       		Scanner in = new Scanner(file);
       
       		int s = in.nextInt();
       		int[] ar = new int[s];
       
       		for(int i=0;i<s;i++){
           		ar[i]=in.nextInt(); 
       		}
       		insertIntoSorted(ar);
       		
       		in.close();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
   }
   
   
   private static void printArray(int[] ar) {
     for(int n: ar){
        System.out.print(n+" ");
     }
       System.out.println("");
   }
   
   public static void insertIntoSorted(int[] ar) {
       // Fill up this function
	   int n = ar.length;
	   int e = ar[n-1];
	   
	   for(int i = n-2; i >= 0; i--){
		   if(i==0){
			   moveElements(ar, e, i);
		   }else{
			   if(e < ar[i] && e > ar[i - 1]){
				   moveElements(ar, e, i);
				   break;
			   }
			   else{
				   ar[i+1] = ar[i];
				   printArray(ar);
			   }
		   }
	   }
   }
   
   public static void moveElements(int[] ar, int e, int index){
	   ar[index+1] = ar[index];
	   printArray(ar);
	   ar[index] = e;
	   printArray(ar);
   }
}
