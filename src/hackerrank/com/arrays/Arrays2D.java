package hackerrank.com.arrays;

import java.io.File;
import java.util.Scanner;

public class Arrays2D {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            int[] array = new int[n];
            int i=0;

            for(i=0;i<n;i++){
                array[i] = in.nextInt();
            }
            for(i=n-1;i>=0;i--){
                System.out.print(array[i] + " ");
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
