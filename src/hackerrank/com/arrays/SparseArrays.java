package hackerrank.com.arrays;

import java.io.File;
import java.util.*;

/**
 *
 */
public class SparseArrays {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            Map<String, Integer> map = new HashMap<String, Integer>();

            int i=0;
            String s = "";

            for(i=0;i<n;i++){
                s = in.next();
                if(map.containsKey(s)){
                    map.put(s, map.get(s) + 1);
                }else{
                    map.put(s, 1);
                }
            }

            int q = in.nextInt();

            for(i=0;i<q;i++){
                s = in.next();
                if(map.containsKey(s)){
                    System.out.println(map.get(s));
                }else{
                    System.out.println(0);
                }
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
