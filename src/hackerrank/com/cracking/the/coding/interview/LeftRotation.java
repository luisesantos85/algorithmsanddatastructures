package hackerrank.com.cracking.the.coding.interview;

import java.io.File;
import java.util.Scanner;

public class LeftRotation {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            int d = in.nextInt(); // left rotations
            int[] array = new int[n];
            int[] result = new int[n];

            for(int i=0;i<n;i++){
                array[i] = in.nextInt();
            }

            if(d>n){
                d = d%n;
            }

            for(int j = 0;j<n;j++){ // for right shifts i would have to fill the result array from n to 0
                result[j] = array[d];
                if(d < n - 1)
                    d++;
                else
                    d=0;
            }

            for(int k =0;k<n;k++)
                System.out.print(result[k] + " ");

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
