package hackerrank.com.strings;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class TwoCharacters {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int size = in.nextInt();
            if (size == 2){
                System.out.println(2);
                return;
            }else if(size == 1){
                System.out.println(0);
                return;
            }

            String s = in.next();
            StringBuilder sb = new StringBuilder(s);

            int index = 0;
            int counter = 0;


            while(true){
                Map<Character, Letter> map = createMap(s);

                if(map == null)
                    break;

                for (Map.Entry<Character, Letter> entry : map.entrySet()) {
                    Character key = entry.getKey();
                    Letter value = entry.getValue();
                    index = 0;

                    while(index < s.length()){
                        char chatAtIndex = s.charAt(index);
                        if(chatAtIndex == key){
                            sb.delete(index, index+1);
                            s = sb.toString();
                            if(index>0)
                                index--;
                        }
                        index++;
                    }
                }
            }

            Map<String, Letter> map = createPairMap(s);
            int max = 0;

            for (Map.Entry<String, Letter> entry : map.entrySet()) {
                String key = entry.getKey();
                Letter value = entry.getValue();

                if(value.qty > max)
                    max = value.qty;
            }
            System.out.print(max);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Map<String, Letter> createPairMap(String s){
        int index = 1;
        Map<String, Letter> result = new HashMap<String, Letter>();

        while(index < s.length()-1){
            Letter l;
            String pair = s.substring(index-1,index+1);
            if(!result.containsKey(pair)){
                l = new Letter();
                l.qty = 1;
                l.candidateToRemove = false;
                result.put(pair, l);
            }else{
                l = result.get(pair);
                l.qty++;
            }
            index++;
        }
        return result;
    }

    public static Map<Character, Letter> createMap(String s){
        Map<Character, Letter> map = new HashMap<Character, Letter>();
        int index = 0;
        while(index < s.length()){
            char second = s.charAt(index);
            Letter l;
            if(!map.containsKey(second)){
                l = new Letter();
                l.qty = 1;
                l.candidateToRemove = false;
                map.put(second, l);
            }else{
                l = map.get(second);
                l.qty++;
            }

            if(index > 0){
                char first = s.charAt(index - 1);
                if(first == second){
                    l.qty++;
                    l.candidateToRemove = true;
                }
            }

            index++;
        }

        Iterator<Map.Entry<Character,Letter>> iter = map.entrySet().iterator();

       while(iter.hasNext()) {

           Map.Entry<Character,Letter> entry = iter.next();
            Character key = entry.getKey();
            Letter value = entry.getValue();

            if(!value.candidateToRemove && value.qty!=1){
                iter.remove();
            }
        }

        if(map.size() == 0)
            return null;
        else
            return map;
    }

    public static class Letter{
        public int qty;
        public boolean candidateToRemove;
    }
}