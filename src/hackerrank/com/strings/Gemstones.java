package hackerrank.com.strings;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Gemstones {

	public static void main(String[] args){
		Gemstones.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int N = Integer.parseInt(input.nextLine());
			HashSet<String> set = null;
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			
			for(int i = 0; i < N; i++){
				String rock = input.nextLine();
				String[] array = rock.split("");
				
				if(i == 0){
					set = new HashSet<String>(Arrays.asList(array));
					Iterator<String> iter = set.iterator();
					while(iter.hasNext()){
						String character = iter.next();
						map.put(character, 1);
					}
					continue;
				}
				
				HashSet<String> aux = new HashSet<String>(Arrays.asList(array));
				Iterator<String> iterAux = aux.iterator();
				while(iterAux.hasNext()){
					String characterAux = iterAux.next();
					if(!map.containsKey(characterAux))
						map.put(characterAux, 1);
					else{
						int counter = map.get(characterAux);
						counter = counter + 1;
						map.put(characterAux, counter);
					}
				}
			}
			
			//Iterate through a HashMap
			int answer = 0;
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				//String key = entry.getKey();
			    Integer value = entry.getValue();
			    if(value == N){
			    	answer++;
			    }
			}		
			
			System.out.println(answer);
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

/**
 * 
 * John has discovered various rocks. Each rock is composed of various elements, and each element is represented by a lower-case Latin letter from 'a' to 'z'. 
 * An element can be present multiple times in a rock. An element is called a gem-element if it occurs at least once in each of the rocks.
 * Given the list of  rocks with their compositions, display the number of gem-elements that exist in those rocks.
 * 
 * Input Format
 * The first line consists of an integer, , the number of rocks. 
 * Each of the next  lines contains a rock's composition. Each composition consists of lower-case letters of English alphabet.
 * 
 * Constraints 
 * Each composition consists of only lower-case Latin letters ('a'-'z'). 
 * length of each composition 
 * 
 * Output Format
 * Print the number of gem-elements that are common in these rocks. If there are none, print 0.
 * 
 * Sample Input
 * 3
 * abcdde
 * baccd
 * eeabg
 * 
 * Sample Output
 * 2
 * 
 * Explanation
 * Only "a" and "b" are the two kinds of gem-elements, since these are the only characters that occur in every rock's composition.
 * 
 * */

