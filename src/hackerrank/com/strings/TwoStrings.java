package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

public class TwoStrings {

	public static void main(String[] args){
		TwoStrings.readInput();
	}
	
	public static void readInput(){
		try{
		
			String abc = "abcdefghijklmnopqrstuvwxyz";
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			
			for(int i = 0;i<T;i++){
				String w1 = input.nextLine();
				String w2 = input.nextLine();
				
				char[] array = abc.toCharArray();
				String result = "NO";
				for(int j=0;j<array.length;j++){
					if(w1.indexOf(array[j]) != -1 && w2.indexOf(array[j]) != -1){
						result = "YES";
						break;
					}
				}
				System.out.println(result);
			}
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}		
	}
}
