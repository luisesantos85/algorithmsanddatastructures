package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

public class Pangrams {

	public static void main(String[] arg){
		Pangrams.readInput();
	}
	
	public static void readInput(){
		try{
			String abc = "abcdefghijklmnopqrstuvwxyz";
			
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			String s = input.nextLine().toLowerCase();
			
			char[] array = abc.toCharArray();
			String answer = "pangram";
			
			for(int i = 0; i< array.length; i++){
				int index = s.indexOf(array[i]);
				if(index == -1){
					answer = "not pangram";
					break;
				}
			}
			System.out.println(answer);
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

/**
 * Roy wanted to increase his typing speed for programming contests. So, his friend advised him to type the sentence "The quick brown fox jumps over the lazy dog" repeatedly, because it is a pangram. 
 * (Pangrams are sentences constructed by using every letter of the alphabet at least once.) 
 * After typing the sentence several times, Roy became bored with it. So he started to look for other pangrams. 
 * Given a sentence , tell Roy if it is a pangram or not.
 * 
 * Input Format
 * Input consists of a string .
 * 
 * Constraints 
 * Length of  can be at most   and it may contain spaces, lower case and upper case letters. Lower-case and upper-case instances of a letter are considered the same.
 * 
 * Output Format
 * Output a line containing pangram if  is a pangram, otherwise output not pangram.
 * 
 * Sample Input
 * 
 * Input #1
 * We promptly judged antique ivory buckles for the next prize    
 * 
 * Input #2
 * We promptly judged antique ivory buckles for the prize    
 * 
 * Sample Output
 * 
 * Output #1
 * pangram
 * 
 * Output #2
 * not pangram
 * 
 * Explanation
 * In the first test case, the answer is pangram because the sentence contains all the letters of the English alphabet.
 * 
 * */
