package hackerrank.com.strings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AlternatingCharacters {
	public static void main(String[] arg){
		AlternatingCharacters.readInput();
	}
	
	public static void readInput(){
		try{
			
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			
			for(int i=0;i<T;i++){
				List<Character> line = new ArrayList<Character>();
				char[] aux = input.nextLine().toCharArray();
				
				for(int k=0;k<aux.length;k++)
					line.add(aux[k]);
				
				char c = line.get(0);
				int counter = 0;
				for(int j = 1; j<line.size();j++){
					if(line.get(j) != c){
						c = line.get(j);
					}else{
						counter++;
					}
				}	
				System.out.println(counter);
			}
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

/**
 * Shashank likes strings in which consecutive characters are different. For example, he likes ABABA, while he doesn't like ABAA. Given a string containing characters 
 * and  only, he wants to change it into a string he likes. To do this, he is allowed to delete the characters in the string.
 * 
 * Your task is to find the minimum number of required deletions.
 * 
 * Input Format
 * The first line contains an integer , i.e. the number of test cases. 
 * The next  lines contain a string each.
 * 
 * Output Format
 * For each test case, print the minimum number of deletions required.
 * 
 * Constraints
 * length of string 
 * 
 * Sample Input
 * 5
 * AAAA
 * BBBBB
 * ABABABAB
 * BABABA
 * AAABBB
 * 
 * Sample Output
 * 3
 * 4
 * 0
 * 0
 * 4
 * 
 * Explanation
 * 
 * AAAA  A, 3 deletions
 * BBBBB  B, 4 deletions
 * ABABABAB  ABABABAB, 0 deletions
 * BABABA  BABABA, 0 deletions
 * AAABBB  AB, 4 deletions because to convert it to AB we need to delete 2 A's and 2 B's
 *  
 * */
 