package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

public class MakeItAnagram {

	public static void main(String[] args){
		MakeItAnagram.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			StringBuilder s1 = new StringBuilder(input.next());
			StringBuilder s2 = new StringBuilder(input.next());
			
			int counter1 = 0;
			int counter2 = 0;
			
			for(int i=0;i<s1.length();i++){
				if(s2.indexOf(s1.charAt(i) + "") < 0){
					counter1++;
				}
			}
			
			for(int j=0;j<s2.length();j++){
				if(s1.indexOf(s2.charAt(j) + "") < 0){
					counter2++;
				}
			}
			
			int left1 = s1.length()-counter1;
			int left2 = s2.length()-counter2;
			int aux = Math.abs(left1-left2);
			
			System.out.println(counter1 + counter2 + aux);
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
