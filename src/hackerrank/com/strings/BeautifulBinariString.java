package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

public class BeautifulBinariString {

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            String s = in.next();
            StringBuilder sb = new StringBuilder(s);
            int counter = 0;

            while(s.contains("010")){

                int index = sb.indexOf("010");
                if(sb.length() > index+3 && sb.charAt(index+3) == '1'){
                    sb.setCharAt(index + 2, '1');
                }else{
                    sb.setCharAt(index + 1, '0');
                }
                s = sb.toString();
                counter++;
            }

            System.out.println(counter);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
/**
 * Alice has a binary string, , of length . She thinks a binary string is beautiful if and only if it doesn't contain the substring .
 * In one step, Alice can change a  to a  (or vice-versa). Count and print the minimum number of steps needed to make Alice see the string as beautiful.
 *
 * Input Format
 * The first line contains an integer,  (the length of binary string ).
 * The second line contains a single binary string, , of length .
 *
 * Output Format
 * Print the minimum number of steps needed to make the string beautiful.
 *
 * Sample Input 0
 * 7
 * 0101010
 *
 * Sample Output 0
 * 2
 *
 * Sample Input 1
 * 5
 * 01100
 * */
