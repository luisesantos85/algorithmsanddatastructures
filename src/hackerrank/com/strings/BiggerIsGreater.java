package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

public class BiggerIsGreater {
	
	public static String stringToPrint = "";
	public static boolean found = false;
	
	public static void main(String[] arg){
		BiggerIsGreater.readInput();
	}
	
	public static void readInput(){
		try{
			String abc = "abcdefghijklmnopqrstuvwxyz";
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			 
			for(int i=0;i<T;i++){
				String w = input.nextLine();
				char charToCompare = 0;
				char movinChar = 0;
				int charToCompareIndex = 0;
				int movingCharIndex = 0;
				boolean found = false;
				StringBuilder s = new StringBuilder(w);
				
				for(int j=w.length()-1;j>=0;j--){
					charToCompare = w.charAt(j);
					for(int k=j-1;k>=0;k--){
						movinChar = w.charAt(k);
						charToCompareIndex = j;
						movingCharIndex = k;
						if(abc.indexOf(movinChar) < abc.indexOf(charToCompare)){
							found = true;
							s.setCharAt(charToCompareIndex, movinChar);
							s.setCharAt(movingCharIndex, charToCompare);
							break;
						}
					}
					if(found)
						break;
				}
				if(found){
					System.out.println(s.toString());
				}else{
					System.out.println("no answer");
				}
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
