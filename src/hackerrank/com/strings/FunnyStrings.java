package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

public class FunnyStrings {
	
	public static void main(String[] arg){
		FunnyStrings.readInput();
	}
	
	public static void readInput(){
		try{
			String abc = "abcdefghijklmnopqrstuvwxyz";
			
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			
			for(int i=0;i<T;i++){
				String S = input.nextLine();
				char[] s = S.toCharArray();
				char[] r = new char[s.length];
				int counter = 0;
				for(int j=s.length-1; j>=0;j--){
					r[counter] = s[j];
					counter++;
				}
				
				String answer = "Funny";
				for(int k=s.length-1; k>0;k--){
					int val1 = Math.abs(abc.indexOf(s[k]) - abc.indexOf(s[k-1]));
					int val2 = Math.abs(abc.indexOf(r[k]) - abc.indexOf(r[k-1]));
					if(val1 != val2){
						answer = "Not Funny";
						break;
					}
				}
				System.out.println(answer);
			}
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

/**
 * Suppose you have a String, , of length  that is indexed from  to . You also have some String, , that is the reverse of String .  is funny if the condition  is true for every character from  to .
 * Note: For some String ,  denotes the ASCII value of the  -indexed character in . The absolute value of an integer, , is written as .
 * 
 * Input Format
 * The first line contains an integer,  (the number of test cases). 
 * Each line  of the  subsequent lines contain a string, .
 * 
 * Constraints
 * 
 * Output Format
 * For each String  (where ), print whether it is  or  on a new line.
 * 
 * Sample Input
 * 2
 * acxz
 * bcxz
 * 
 * Sample Output
 * Funny
 * Not Funny
 * 
 * */
 