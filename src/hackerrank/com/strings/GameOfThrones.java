package hackerrank.com.strings;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class GameOfThrones {
	public static void main(String[] arg){
		GameOfThrones.readInput();
	}
	
	public static void readInput(){
		try{
			
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			char[] line = input.nextLine().toCharArray();
			HashMap<Character, Integer> map = new HashMap<Character,Integer>();
			
			for(int i=0;i<line.length;i++){
				if(map.containsKey(line[i])){
					map.put(line[i], map.get(line[i]) + 1);
				}else{
					map.put(line[i], 1);
				}
			}
			
			Iterator<Entry<Character, Integer>> it = map.entrySet().iterator();
			boolean oddFound = false;
			String result = "YES";
			while (it.hasNext()) {
				Map.Entry<Character, Integer> pair = (Map.Entry<Character, Integer>)it.next();
				int value = (Integer)pair.getValue();
				if(value%2 != 0){
					if(oddFound){
						result = "NO";
						break;
					}else{
						oddFound = true;
					}
				}
			}
			System.out.println(result);
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
