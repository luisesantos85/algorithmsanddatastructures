package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

/**
 * Created by luis on 11/21/16.
 */
public class MarsExploration {

    public static String sos = "SO";

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            String s = in.next();
            int size = s.length();
            int counter = 0;
            int index = 0;

            while(index < size){
                char first = s.charAt(index);
                index++;
                char second = s.charAt(index);
                index++;
                char third = s.charAt(index);
                index++;

                if(first != 'S')
                    counter++;
                if(second != 'O')
                    counter++;
                if(third != 'S')
                    counter++;
            }

            System.out.println(counter);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
