package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

public class CamelCase {

    public static String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            String s = in.next();
            int size = s.length();
            int count = (size==0)?0:1;

            for(int i=0;i<size;i++){
                if(upper.contains(Character.toString(s.charAt(i)))){
                    count++;
                }
            }

            System.out.println(count);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
