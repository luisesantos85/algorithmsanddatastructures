package hackerrank.com.strings;

import java.io.File;
import java.util.Scanner;

public class SuperReducedStrings {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            String s = in.next();
            StringBuilder sb = new StringBuilder(s);
            int index = 0;

            while(index < s.length()){
                if(index + 1 < s.length()){
                    char first = s.charAt(index);
                    char second = s.charAt(index + 1);
                    if(first == second){
                        sb.delete(index, index+1);
                        sb.delete(index, index+1);
                        s = sb.toString();
                        index=0;
                    }else{
                        index++;
                    }
                }else{
                    break;
                }
            }

            if(s.length() == 0){
                System.out.println("Empty String");
            }else{
                System.out.println(s);
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

/**
 * Steve has a string, , consisting of  lowercase English alphabetic letters. In one operation, he can delete any pair of adjacent letters with same value. For example, string "aabcc" would become either "aab" or "bcc" after operation.
 * Steve wants to reduce  as much as possible. To do this, he will repeat the above operation as many times as it can be performed. Help Steve out by finding and printing 's non-reducible form!
 *
 * Note: If the final string is empty, print Empty String .
 *
 * Input Format
 * A single string, .
 *
 * Output Format
 * If the final string is empty, print Empty String; otherwise, print the final non-reducible string.
 *
 * Sample Input 0
 * aaabccddd
 *
 * Sample Output 0
 * abd
 * */