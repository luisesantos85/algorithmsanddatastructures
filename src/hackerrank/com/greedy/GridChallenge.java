package hackerrank.com.greedy;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;


public class GridChallenge {

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int T = in.nextInt();

            while(T>0){

                int N = in.nextInt();

                char[][] G = new char[N][N];
                int i=0;
                int j=0;

                for(i=0;i<N;i++){
                    G[i] = in.next().toCharArray();
                    Arrays.sort(G[i]);
                }

                boolean isOrdered = true;

                for(i=0;i<N;i++){
                    for(j=0;j<N-1;j++){
                        if(G[j][i] > G[j+1][i]){
                            isOrdered = false;
                            break;
                        }
                    }
                    if(!isOrdered)
                        break;
                }
                System.out.println((isOrdered)?"YES":"NO");
                T--;
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
/**
 * https://www.hackerrank.com/challenges/grid-challenge
 */
