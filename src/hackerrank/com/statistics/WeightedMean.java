package hackerrank.com.statistics;

import java.io.File;
import java.util.Scanner;

/**
 * Created by luisesantos on 11/13/16.
 */
public class WeightedMean {

    public static void main(String[] args) {
        readInput();
    }

    public static void readInput() {
        try {
            File file = new File("/Users/luisesantos/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);

            int N = input.nextInt();
            float[] array = new float[N];
            float[] weights = new float[N];
            float ws = 0;
            float as=0;

            for(int i =0;i<N;i++){
                array[i] = input.nextFloat();
            }
            for(int j =0;j<N;j++){
                float item = input.nextFloat();
                weights[j] = item;
                ws += item;
            }

            for(int k =0;k<N;k++){
                as += array[k]*weights[k];
            }

            System.out.printf("%.1f",as/ws);
            //System.out.println(as/ws);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}