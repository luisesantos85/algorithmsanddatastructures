package hackerrank.com.statistics;

import main.algorithms.sort.MergeSort;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MeanMedianAndMode {

    public static void main(String[] args) {
        readInput();
    }

    public static void readInput() {
        try {
            File file = new File("/Users/luisesantos/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);

            int N = input.nextInt();
            Map<Integer, Integer> map = new HashMap<Integer, Integer>();
            int[] X = new int[N];

            float mean = 0;
            float mode = Float.MAX_VALUE;
            float median;

            for(int i=0;i<N;i++){
                int item = input.nextInt();
                X[i] = item;
                mean += item;

                if(map.containsKey(item)){
                    map.put(item, map.get(item) + 1);
                }else{
                    map.put(item, 1);
                }
            }
            int max = Integer.MIN_VALUE;

            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                Integer key = entry.getKey();
                Integer value = entry.getValue();
                if(value > max){
                    max = value;
                    mode = key;
                }else if(value == max){
                    if(key < mode){
                        mode = key;
                        max = value;
                    }
                }
            }

            mean = mean/N;
            Arrays.sort(X);

            if(X.length%2==0){
                //is even
                int aux1 = X.length/2;
                median = X[aux1] + X[aux1 - 1];
                median = median/2;
            }else{
                //is odd
                float aux2 = X.length/2;
                median = X[(int)Math.ceil(aux2)];
            }

            if(mean - (int)mean == 0)
                System.out.println((int)mean);
            else{
                System.out.printf("%.1f",mean);
                System.out.println();
            }

            if(median - (int)median == 0){
                System.out.println((int)median);
            }else{
                System.out.printf("%.1f", median);
                System.out.println();
            }

            if(mode - (int)mode == 0)
                System.out.println((int)mode);
            else{
                System.out.printf("%.1f",mode);
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
