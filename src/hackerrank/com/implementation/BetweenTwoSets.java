package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class BetweenTwoSets {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try {
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            int m = in.nextInt();
            int maxA=Integer.MIN_VALUE;
            int minB=Integer.MAX_VALUE;

            int[] a = new int[n];
            for(int a_i=0; a_i < n; a_i++){
                a[a_i] = in.nextInt();
                if(a[a_i]>maxA)
                    maxA = a[a_i];
            }

            int[] b = new int[m];
            for(int b_i=0; b_i < m; b_i++){
                b[b_i] = in.nextInt();
                if(b[b_i]<minB)
                    minB = b[b_i];
            }

            int result = 0;
            for(int i = maxA;i<=minB;i++){
                boolean divisibleA = true;
                boolean divisibleB = true;

                for(int j=0;j<a.length;j++){
                    if(i%a[j]!=0){
                        divisibleA = false;
                        break;
                    }
                }

                if(divisibleA){
                    for(int k=0;k<b.length;k++){
                        if(b[k]%i != 0){
                            divisibleB = false;
                            break;
                        }
                    }
                }

                if(divisibleA && divisibleB)
                    result++;

            }

            System.out.print(result);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

/**
 * Consider two sets of positive integers,  and . We say that a positive integer, , is between sets  and  if the following conditions are satisfied:
 *
 * All elements in  are factors of .
 * is a factor of all elements in .
 * Given  and , find and print the number of integers (i.e., possible 's) that are between the two sets.
 *
 * Input Format
 * The first line contains two space-separated integers describing the respective values of  (the number of elements in set ) and  (the number of elements in set ).
 * The second line contains  distinct space-separated integers describing .
 * The third line contains  distinct space-separated integers describing .
 *
 * Output Format
 * Print the number of integers that are considered to be between  and .
 *
 * Sample Input
 * 2 3
 * 2 4
 * 16 32 96
 *
 * Sample Output
 * 3
 */
