package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class CavityMap {

	public static void main(String[] args) {
		CavityMap.readInput();
	}
	
	public static void printMatrix(char[][] matrix, int N){
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				System.out.print(matrix[i][j]);
			}
			System.out.println();
		}
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int N = Integer.parseInt(input.nextLine());
			char[][] matrix = new char[N][N];
			if(N < 1 || N > 100){
				input.close();
				throw new IllegalArgumentException();
			}
			
			int counter = 0;
			while(input.hasNextLine()){
				String line = input.nextLine(); 
				char[] aux = line.toCharArray();
				matrix[counter] = aux;
				counter++;
			}
			
			if(N > 2){
				for(int i=0;i<N;i++){
					for(int j=0;j<N;j++){
						if(i==0){
							if(j==N-1){
								System.out.println(matrix[i][j]);
							}else{
								System.out.print(matrix[i][j]);
							}
							continue;
						}else if(i==N-1){
							System.out.print(matrix[i][j]);
							continue;
						}else{
							if(j==0){
								System.out.print(matrix[i][j]);
								continue;
							}
							if(j==N-1){
								System.out.println(matrix[i][j]);
								continue;
							}
						}
						
						int value = Character.getNumericValue(matrix[i][j]);
						int left = Character.getNumericValue(matrix[i][j-1]);
						int right = Character.getNumericValue(matrix[i][j+1]);
						int up = Character.getNumericValue(matrix[i-1][j]);
						int down = Character.getNumericValue(matrix[i+1][j]);
						
						if(up < value && down < value && left < value && right < value){
							System.out.print('X');
						}else{
							System.out.print(matrix[i][j]);
						}
					}
				}
				
			}else{
				CavityMap.printMatrix(matrix, N);
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
