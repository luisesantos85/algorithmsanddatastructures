package hackerrank.com.implementation;

import java.io.File;
import java.math.BigInteger;
import java.util.Scanner;

public class TaumAndBday {

	public static void main(String[] args){
		TaumAndBday.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			if(T < 1 || T > 10){
				input.close();
				throw new IllegalArgumentException();
			}
			
			for(int i=0;i<T;i++){
				String[] aux = input.nextLine().split(" ");
				BigInteger B = new BigInteger(aux[0]);
				BigInteger W = new BigInteger(aux[1]);
				
				String[] aux2 = input.nextLine().split(" ");
				BigInteger x = new BigInteger(aux2[0]);
				BigInteger y = new BigInteger(aux2[1]);
				BigInteger z = new BigInteger(aux2[2]);
				
				BigInteger costB;
				BigInteger costW;
				
				if(x.add(z).compareTo(y) == -1){
					costB = B.multiply(x);
					costW = W.multiply(x).add(W.multiply(z));
				}else if(y.add(z).compareTo(x) == -1){
					costB = B.multiply(z).add(B.multiply(y));
					costW = W.multiply(y);
				}else{
					costB = B.multiply(x);
					costW = W.multiply(y);
				}
				System.out.println(costB.add(costW));
			}
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
