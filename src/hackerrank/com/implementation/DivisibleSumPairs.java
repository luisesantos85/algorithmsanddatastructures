package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class DivisibleSumPairs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DivisibleSumPairs.readInput();
	}

	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner in = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int n = in.nextInt();
			int k = in.nextInt();
			
			int[] array = new int[n];
			for(int i = 0; i < n ; i ++){
				array[i] = in.nextInt();
			}
			int result = 0;
			for(int j=0;j<n;j++){
				for(int l=j;l<n;l++){
					if(j==l)
						continue;
					
					int sum = array[j] + array[l]; 
					if(sum%k ==0)
						result++;
				}
			}
	        System.out.println(result);
			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
