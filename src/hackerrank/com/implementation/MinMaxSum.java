package hackerrank.com.implementation;

import java.io.File;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

public class MinMaxSum {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);
            //Scanner in = new Scanner(System.in);

            int N = 5;
            BigInteger[] array = new BigInteger[N];

            for(int i = 0;i<N;i++){
                array[i] = in.nextBigInteger();
            }

            Arrays.sort(array);
            BigInteger min = new BigInteger("0");
            BigInteger max = new BigInteger("0");

            for(int j=0;j<N-1;j++){
                min = min.add(array[j]);
            }

            for(int j=1;j<N;j++){
                max = max.add(array[j]);
            }

            System.out.println(min.toString() + " " + max.toString());

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
/**
 * Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers. Then print the respective minimum and maximum values as a single line of two space-separated long integers.
 *
 * Input Format
 *
 * A single line of five space-separated integers.
 *
 * Constraints
 * Each integer is in the inclusive range .
 *
 * Output Format
 * Print two space-separated long integers denoting the respective minimum and maximum values that can be calculated by summing exactly four of the five integers. (The output can be greater than 32 bit integer.)
 *
 * Sample Input
 * 1 2 3 4 5
 *
 * Sample Output
 * 10 14
 *
 * */