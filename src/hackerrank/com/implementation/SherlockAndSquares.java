package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class SherlockAndSquares {

	public static void main(String[] args){
		SherlockAndSquares.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = input.nextInt();
			if(T < 1 || T > 100){
				input.close();
				throw new IllegalArgumentException();
			}
			
			
			while(input.hasNextLine()){				
				double A = new Double(Math.sqrt(input.nextDouble()));
				double B = new Double(Math.sqrt(input.nextDouble()));
				
				double result = Math.floor(B - A);
				
				if(A%1 == 0)result++;
				if(B%1 == 0)result++;
				
				System.out.println(new Double(result).intValue());
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
