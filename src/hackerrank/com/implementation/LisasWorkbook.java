package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class LisasWorkbook {

	public static void main(String[] args){
		LisasWorkbook.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);

			String[] line = input.nextLine().split(" ");
			int n = Integer.parseInt(line[0]);
			int k = Integer.parseInt(line[1]);
			
			String[] problemsPerChapter = input.nextLine().split(" ");
			int currentPage = 1;
			int answer = 0;
			
			for(int i=0;i<n;i++){
				int problems = Integer.parseInt(problemsPerChapter[i]);
				boolean changeInLast = false;
				for(int j=1;j<=problems;j++){
					changeInLast = false;
					if(j==currentPage)
						answer++;
					if(j%k==0){
						currentPage++;
						changeInLast = true;
					}
				}
				if(!changeInLast)
					currentPage++;
			}
			
			System.out.println(answer);
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
