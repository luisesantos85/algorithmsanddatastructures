package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class AppleAndOrange {

	public static void main(String[] args) {
		AppleAndOrange.readInput();
	}

	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner in = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int s = in.nextInt();
	        int t = in.nextInt();
	        int a = in.nextInt();
	        int b = in.nextInt();
	        int m = in.nextInt();
	        int n = in.nextInt();
	        int apples = 0;
	        int oranges = 0;
	        int d=0;

	        for(int apple_i=0; apple_i < m; apple_i++){
	        	d = in.nextInt();
	        	int ax = a + d;
	        	if(ax >= s && ax <= t){
	        		apples++;
	        	}
	        }

	        for(int orange_i=0; orange_i < n; orange_i++){
	        	d = in.nextInt();
	        	int bx = b + d;
	        	if(bx >= s && bx <= t){
	        		oranges++;
	        	}
	        }
			
	        System.out.println(apples);
	        System.out.println(oranges);
	        
			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
