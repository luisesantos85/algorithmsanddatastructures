package hackerrank.com.implementation;

import java.io.File;
import java.math.BigInteger;
import java.util.Scanner;

public class BeautifulDaysAtTheMovies {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);
            //Scanner in = new Scanner(System.in);

            BigInteger i = in.nextBigInteger();
            BigInteger j = in.nextBigInteger();
            BigInteger k = in.nextBigInteger();

            BigInteger l = new BigInteger(i.toString());

           // for (; bigI.compareTo(one) == 0; bigI.subtract(one)) {
           //     bigI = bigI.add(one);
           // }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

/**
 * https://www.hackerrank.com/challenges/beautiful-days-at-the-movies
 * */