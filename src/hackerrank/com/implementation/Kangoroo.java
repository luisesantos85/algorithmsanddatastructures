package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class Kangoroo {
	public static void main(String[] args) {
		Kangoroo.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner in = new Scanner(file);
			
			//Scanner in = new Scanner(System.in);
			int x1=in.nextInt();
			int v1=in.nextInt();
			
			int x2=in.nextInt();
			int v2=in.nextInt();
			
			int xmin = 0;
			int vmin = 0;
			
			int xmax = 0;
			int vmax = 0;
			
			boolean resulted = false;
			
			if(x1 > x2){
				xmin=x2;
				vmin=v2;
				xmax=x1;
				vmax=v1;
			}else if(x2 > x1){
				xmin=x1;
				vmin=v1;
				xmax=x2;
				vmax=v2;
			}else{
				System.out.println("YES");
				resulted = true;
			}
			
			if(vmax > vmin){
				System.out.println("NO");
				resulted = true;
			}else{
				while(xmin < xmax){
					xmin += vmin;
					xmax += vmax;
					
					if(xmin==xmax){
						System.out.println("YES");
						resulted = true;
					}
						
				}
			}
			
			if(!resulted)
				System.out.println("NO");
			
			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
