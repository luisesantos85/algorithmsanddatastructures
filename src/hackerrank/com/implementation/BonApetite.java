package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class BonApetite {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            int k = in.nextInt();
            int[] prices = new int[n];
            int fair = 0;

            for(int i=0; i<n;i++){
                int value = in.nextInt();
                prices[i] = value;
                if(i!=k)
                    fair += value;
            }

            fair = fair/2;
            int charged = in.nextInt();

            if(fair==charged)
                System.out.println("Bon Appetit");
            else
                System.out.println(charged-fair);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
