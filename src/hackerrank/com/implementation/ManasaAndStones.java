package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class ManasaAndStones {

	public static void main(String args[]){
		ManasaAndStones.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			
			for(int i=0;i<T;i++){
				int n = Integer.parseInt(input.nextLine());
				int a = Integer.parseInt(input.nextLine());
				int b = Integer.parseInt(input.nextLine());
				
				int difference = Math.abs(b - a);
				
				int max = Math.max(a, b)*(n-1);
				int current = Math.min(a, b)*(n-1);
				
				if(a == b){
					System.out.print(current);
				}
				else{
					while (current <= max){
						System.out.print(current + " ");
						current += difference;
					}
				}
				System.out.println();
			}
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	//NOTE: If you have problems understanding the problem, take a look to the output
}
