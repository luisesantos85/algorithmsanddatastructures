package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class SongOfPI {

	public static void main(String[] args){
		SongOfPI.readInput();
	}
	
	public static void readInput(){
		try{
			String PI = "31415926535897932384626433833";
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			if(T < 1 || T > 100){
				input.close();
				throw new IllegalArgumentException();
			}
			
			while(input.hasNextLine()){
				String[] line = input.nextLine().split(" ");
				String aux = "";
				for(int i=0;i<line.length;i++){
					aux += line[i].length();
				}
				String big = PI.substring(0, aux.length());
				if(big.equals(aux)){
					System.out.println("It's a pi song.");
				}else{
					System.out.println("It's not a pi song.");
				}
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
