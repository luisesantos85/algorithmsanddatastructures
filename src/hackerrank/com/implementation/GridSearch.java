package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class GridSearch {

	public static void main(String[] args) {
		GridSearch.readInput();
	}
	
	public static void readInput(){
		try{
			//Scanner input = new Scanner(System.in);
			
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			int T = Integer.parseInt(input.nextLine());
			
			if(T < 1 || T > 5){
				input.close();
				throw new IllegalArgumentException();
			}
			
			for(int i=0; i<T;i++){
				String[] line = input.nextLine().split(" ");
				int R = Integer.parseInt(line[0]);
				//int C = Integer.parseInt(line[1]);
				String[] G = new String[R];
				
				for(int j=0; j<R;j++){
					G[j] = input.nextLine();
				}
				
				String[] line2 = input.nextLine().split(" ");
				int r = Integer.parseInt(line2[0]);
				//int c = Integer.parseInt(line2[1]);
				String[] P = new String[r];
				
				for(int k=0; k<r;k++){
					P[k] = input.nextLine();
				}
				
				int counter = 0;
				//boolean found = false;
				int currentIndex = -1;
				String answer = "NO";
				for(int o=0; o<R;o++){
					if(currentIndex == -1){
						if(G[o].indexOf(P[counter]) > -1){
							currentIndex = G[o].indexOf(P[counter]);
							counter++;
							
							if(counter == r){
								answer = "YES";
								break;
							}
						}else{
							counter = 0;
							currentIndex = -1;
						}
					}else{
						if(G[o].indexOf(P[counter]) > -1 && G[o].indexOf(P[counter]) == currentIndex){
							counter++;
							
							if(counter == r){
								answer = "YES";
								break;
							}
						}else{
							counter = 0;
							currentIndex = -1;
						}
					}
					
				}
				System.out.println(answer);
			}
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
