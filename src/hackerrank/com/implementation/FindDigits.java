package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class FindDigits {	
	public static void main(String[] args){
		FindDigits.readInput();
	}
	
	public static void readInput(){
		try{
			
			File file = new File("/Users/luisesantos/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			if(T < 1 || T > 15){
				input.close();
				throw new IllegalArgumentException();
			}
			
			double top = Math.pow(10, 10);
			while(input.hasNextLine()){
				String line = input.nextLine(); 
				char[] aux = line.toCharArray();
				int N = Integer.parseInt(line);
				int counter=0;
				if(N < 1 || N > top){
					input.close();
					throw new IllegalArgumentException();
				}
				for(int i=0;i<aux.length;i++){
					int number = Integer.parseInt(aux[i] + "");
					if(number!=0){
						if(N%number == 0)
							counter++;
					}
				}
				System.out.println(counter);
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
