package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class Encryption {
	public static void main(String[] args){
		Encryption.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			String t = input.nextLine();
			int L = t.length();
			
			if(L > 81){
				input.close();
				throw new IllegalArgumentException();
			}
			
			Double floor = Math.floor(Math.sqrt(L));
			Double ceiling = Math.ceil(Math.sqrt(L));

			if(floor*ceiling < L)
				floor = ceiling;
			
			String[][] matrix = new String[floor.intValue()][ceiling.intValue()];
			int counter = 0;
			for(int i=0;i<floor.intValue();i++){
				for(int j=0;j<ceiling.intValue();j++){
					if(counter<L){
						matrix[i][j] = t.charAt(counter) + "";
						counter++;
					}else{
						break;
					}
				}
			}
			
			for(int j=0;j<ceiling.intValue();j++){
				for(int i=0;i<floor.intValue();i++){
					if(matrix[i][j] != null)
						System.out.print(matrix[i][j]);
				}
				System.out.print(" ");
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
