package hackerrank.com.implementation;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SockMerchant {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luisesantos/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            Map<Integer, Integer> map = new HashMap<Integer, Integer>();

            for(int i=0; i<n;i++){
                int color = in.nextInt();

                if(map.containsKey(color)){
                    map.put(color, map.get(color) + 1);
                }else{
                    map.put(color, 1);
                }
            }

            int result = 0;
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                Integer color = entry.getKey();
                Integer amount = entry.getValue();

                if(amount < 2)
                    continue;

                if(amount%2 == 0)
                    result += amount/2;
                else {
                    int aux = amount - 1;
                    result += aux/2;
                }
            }

            System.out.println(result);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
