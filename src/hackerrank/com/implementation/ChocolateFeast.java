package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class ChocolateFeast {

	public static void main(String[] args) {
		try{
			File file = new File("/Users/luisesantos/dev/input.txt");
			Scanner in = new Scanner(file);
			
	        //Scanner in = new Scanner(System.in);
	        int t = in.nextInt();
	        for(int i = 0; i < t; i++){
	            System.out.println(Solve(in.nextInt(), in.nextInt(), in.nextInt()));
	        }
	        in.close();	
		}catch(Exception e){
			e.printStackTrace();
		}
    }
    
    private static long Solve(int N, int C, int M){
         //Write code to solve each of the test over here
        if(N < 2 || N > Math.pow(10, 5) || C < 1 || C > N || M < 2 || M > N){
			throw new IllegalArgumentException();
		}
				
		int chocolates = N/C;
		return chocolates + getChocolatesFromWrappers(chocolates, M);
    }
    
    private static long getChocolatesFromWrappers(int wrappers, int M){
    	int free = wrappers / M;
    	int freeRes = wrappers % M;
    	if(freeRes + free >= M){
    		return free + getChocolatesFromWrappers(freeRes + free, M);
    	}else{
    		return free;
    	}    	
    }
}