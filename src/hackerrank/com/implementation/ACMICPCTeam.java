package hackerrank.com.implementation;

import java.io.File;
import java.util.Scanner;

public class ACMICPCTeam {

	public static void main(String[] args) {
		ACMICPCTeam.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			String[] line = input.nextLine().split(" ");
			int N = Integer.parseInt(line[0]);
			int M = Integer.parseInt(line[1]);
			int counter =0;
			String[] people = new String[N];
			
			for(int i=0;i<N;i++){
				people[i] = input.nextLine();
			}
			
			int maxTopics = 0;
			int teams = 0;
			
			for(int i=0;i<N;i++){
				for(int j=counter;j<N;j++){
					
						char[] p1 = people[i].toCharArray();
						char[] p2 = people[j].toCharArray();
						
						int m = 0;
						for(int k=0;k<M;k++){
							if(p1[k] == '1' || p2[k] == '1'){
								m++;
							}
						}
						if(m > maxTopics){
							maxTopics = m;
							teams = 1;
						}else if(m == maxTopics){
							teams++;
						}
					
				}
				counter++;
			}
			System.out.println(maxTopics);
			System.out.println(teams);
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
