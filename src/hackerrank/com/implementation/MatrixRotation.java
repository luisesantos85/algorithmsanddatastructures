package hackerrank.com.implementation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MatrixRotation {

	public static void main(String[] args) {
		MatrixRotation.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			String[] line = input.nextLine().split(" ");
			int M = Integer.parseInt(line[0]);
			int N = Integer.parseInt(line[1]);
			long R = Long.parseLong(line[2]);
			
			if(M < 2 || M > 300 || N < 2 || N > 300){
				input.close();
				throw new IllegalArgumentException();
			}
			
			String[][] matrix = new String[M][N];
			String[][] result = new String[M][N];
			for(int u =0;u<M;u++){
				String[] row = input.nextLine().split(" ");
				matrix[u] = row;
			}
			
			int middle = (M < N)?M/2:N/2;
			
			
			for(int i=0;i<middle;i++){
				List<String> list = new ArrayList<String>();
				
				for(int j=i;j<M-i;j++){
					list.add(matrix[j][i]);
				}
				
				for(int k=i;k<N-i-1;k++){
					list.add(matrix[M - i - 1][k + 1]);
				}
				
				for(int l=M-i-1;l>i;l--){
					list.add(matrix[l-1][N-i-1]);
				}
				
				for(int o=N-i-1;o>i+1;o--){
					list.add(matrix[i][o-1]);
				}
				
				String[] aux = new String[list.size()];
				aux = MatrixRotation.rotate(list, R);
				
				int counter = 0;
				
				for(int j=i;j<M-i;j++){
					result[j][i] = aux[counter];
					counter++;
				}
				
				for(int k=i;k<N-i-1;k++){
					result[M - i - 1][k + 1] = aux[counter];
					counter++;
				}
				
				for(int l=M-i-1;l>i;l--){
					result[l-1][N-i-1] = aux[counter];
					counter++;
				}
				
				for(int o=N-i-1;o>i+1;o--){
					result[i][o-1] = aux[counter];
					counter++;
				}
				
			}
			for(int i=0;i<M;i++){
				for(int j=0;j<N;j++){
					System.out.print(result[i][j] + " ");
				}
				System.out.println();
			}
			
			
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static String[] rotate(List<String> list, long steps){
		String[] result = new String[list.size()];
		int size = list.size();
		
		while(steps > size){
			steps = steps - size;
		}
		
		int s = (int)steps;
		
		for(int i=0; i<size;i++){
			if(i+s < size){
				result[i+s] = list.get(i);
			}else{
				int n = i + s;
				n = size - n;
				result[Math.abs(n)] = list.get(i);
			}
			
		}
		
		return result;
	}
}
