package hackerrank.com.implementation;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class SherlockAndBeast {

	public static void main(String[] args) {
		SherlockAndBeast.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = input.nextInt();
			
			if(T > 20 || T< 1){
				input.close();
				throw new IllegalArgumentException();
			}
			
			while(input.hasNextLine()){
				int N = input.nextInt();
				if(N > 100000 || N < 1){
					input.close();
					throw new IllegalArgumentException();
				}
				//Best way to create big strings. Never use for loops and +=
				char[] fives = new char[N];
				Arrays.fill(fives, '5');
				String result = new String(fives);
				
				int pivot = SherlockAndBeast.getPivot(N);
				if(pivot < 0)
					System.out.println("-1");
				else{
					result=result.substring(0,pivot);
					char[] threes = new char[N-pivot];
					Arrays.fill(threes, '3');

					String threeString = new String(threes);
					result += threeString;
					System.out.println(result);
				}
			}	
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Finds a pivot
	 * */
	public static int getPivot(int N){
		while(N > 0) {
	        if(N % 3 == 0)
	            break;
	        else
	            N -= 5;
	    }
	    return N;
	}
}

/**
 * 
 * Sherlock Holmes suspects his archenemy, Professor Moriarty, is once again plotting something diabolical. Sherlock's companion, Dr. Watson, suggests Moriarty may be responsible for MI6's recent issues with their supercomputer, The Beast.
 * Shortly after resolving to investigate, Sherlock receives a note from Moriarty boasting about infecting The Beast with a virus; however, he also gives him a clue—a number, . Sherlock determines the key to removing the virus is to find the largest Decent Number having  digits.
 * 
 * A Decent Number has the following properties:
 * 
 * Its digits can only be 3's and/or 5's.
 * The number of 3's it contains is divisible by 5.
 * The number of 5's it contains is divisible by 3.
 * If there are more than one such number, we pick the largest one.
 * Moriarty's virus shows a clock counting down to The Beast's destruction, and time is running out fast. Your task is to help Sherlock find the key before The Beast is destroyed!
 * 
 * Constraints:
 * Input Format
 * The first line is an integer, , denoting the number of test cases.
 * The  subsequent lines each contain an integer, , detailing the number of digits in the number.
 * 
 * Output Format
 * Print the largest Decent Number having  digits; if no such number exists, tell Sherlock by printing -1.
 * 
 * Sample Input:
 * 4
 * 1
 * 3
 * 5
 * 11
 * 
 * Sample Output:
 * -1
 * 555
 * 33333
 * 55555533333
 * 
 * */
