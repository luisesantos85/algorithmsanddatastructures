package hackerrank.com.tutorials;


import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Regex {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            ArrayList<String> list = new ArrayList<String>();
            Pattern pattern = Pattern.compile("(\\W|^)[\\w.+\\-]*@gmail\\.com(\\W|$)");

            int n = in.nextInt();
            for(int i = 0 ; i < n ; i++) {
                String name = in.next();
                String email = in.next();

                if (pattern.matcher(email).matches())
                    list.add(name);
            }

            Collections.sort(list);

            for(int j =0; j<list.size();j++){
                System.out.println(list.get(j));
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
