package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

public class Sorting {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            int[] a = new int[n];

            for(int i=0;i<n;i++){
                a[i] = in.nextInt();
            }

            int totalSwaps = 0;
            int numberOfSwaps = 0;

            for (int i = 0; i < n; i++) {
                numberOfSwaps = 0;
                for (int j = 0; j < n - 1; j++) {
                    if (a[j] > a[j + 1]) {
                        //swap(a[j], a[j + 1]);

                        int aux = a[j];
                        a[j] = a[j+1];
                        a[j+1] = aux;

                        numberOfSwaps++;
                        //totalSwaps += numberOfSwaps;
                        totalSwaps++;
                    }
                }
                if (numberOfSwaps == 0) {
                    break;
                }
            }

            System.out.println("Array is sorted in " + totalSwaps + " swaps.");
            System.out.println("First Element: " + a[0]);
            System.out.println("Last Element: " + a[n-1]);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
