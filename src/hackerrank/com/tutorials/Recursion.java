package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

public class Recursion {

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int N = in.nextInt();

            System.out.print(factorial(N));

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static int factorial(int N){
        if(N==1)
            return N;
        else
            return N*factorial(N-1);
    }
}
