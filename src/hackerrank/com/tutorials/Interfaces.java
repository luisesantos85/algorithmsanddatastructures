package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;


interface AdvancedArithmetic{
    int divisorSum(int n);
}

class Calculator implements AdvancedArithmetic{
    public int divisorSum(int n){
        int result = 0;
        for(int i = 0; i<=n;i++){
            if(n%i==0)
                result += i;
        }
        return result;
    }
}

public class Interfaces {

    public static void main(String[] args) {
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner scan = new Scanner(file);

            //Scanner scan = new Scanner(System.in);

            int n = scan.nextInt();
            scan.close();

            AdvancedArithmetic myCalculator = new Calculator();
            int sum = myCalculator.divisorSum(n);
            System.out.println("I implemented: " + myCalculator.getClass().getInterfaces()[0].getName() );
            System.out.println(sum);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

