package hackerrank.com.tutorials;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by luisesantos on 11/20/16.
 */
public class DictionariesAndMaps {

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try {
            File file = new File("/Users/luisesantos/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);

            int n = input.nextInt();

            Map<String, String> phoneBook = new HashMap<String, String>();

            for(int i=0;i<n;i++){
                phoneBook.put(input.next(), input.next());
            }
            while(input.hasNext()){
                String key = input.next();
                if(!phoneBook.containsKey(key)){
                    System.out.println("Not Found");
                }else{
                    System.out.println(key+"="+phoneBook.get(key));
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
