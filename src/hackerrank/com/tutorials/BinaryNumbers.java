package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

public class BinaryNumbers {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int n = in.nextInt();
            String binary = Integer.toString(n, 2);

            int size = binary.length();
            int counter = 0;
            int maximum = 0;

            for(int i=0;i<size;i++){
                char digit = binary.charAt(i);
                if(digit=='1'){
                    counter++;
                }else{
                    if(counter > maximum)
                        maximum = counter;
                    counter=0;
                }
            }

            if(counter > maximum)
                maximum = counter;

            System.out.println(maximum);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
