package hackerrank.com.tutorials;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class BSTLevelOrderTraversal {

    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root=insert(root,data);
        }
        levelOrder(root);
    }

    static void levelOrder(Node root){

        Queue<Node> queue=new LinkedList<Node>();
        queue.add(root);

        while(!queue.isEmpty()){
            Node tempNode=queue.poll();
            System.out.printf("%d ",tempNode.data);
            if(tempNode.left!=null)
                queue.add(tempNode.left);
            if(tempNode.right!=null)
                queue.add(tempNode.right);
        }
    }

    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }
}

class Node{
    Node left,right,next;
    int data;
    Node(int data){
        this.data=data;
        left=right=null;
    }
}
