package hackerrank.com.tutorials;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class TwoDArray {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);
            int n = 6;
            int [][] array = new int[n][n];

            for(int i = 0; i < n; i++){
                for(int j = 0; j < n; j++){
                    array[j][i] = in.nextInt();
                }
            }

            int indexX=0;
            int indexY=0;
            int sum = 0;
            int[] sumArray = new int[16];

            for(int k=0;k<16;k++){
                sum = 0;
                sum = array[indexX][indexY] + array[indexX+1][indexY] + array[indexX+2][indexY] + array[indexX+1][indexY+1] + array[indexX][indexY+2] + array[indexX+1][indexY + 2] + array[indexX+2][indexY + 2];
                sumArray[k] = sum;

                if(indexX == 3){
                    indexY++;
                    indexX = 0;
                }else{
                    indexX++;
                }
            }

            Arrays.sort(sumArray);


            System.out.println(sumArray[15]);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
