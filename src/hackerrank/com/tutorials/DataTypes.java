package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

/**
 * Created by luisesantos on 11/13/16.
 */
public class DataTypes {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try {
            int i = 4;
            double d = 4.0;
            String s = "HackerRank ";

            File file = new File("/Users/luisesantos/dev/input.txt");
            Scanner scan = new Scanner(file);

            //Scanner input = new Scanner(System.in);

            int j = Integer.parseInt(scan.nextLine());
            double e = Double.parseDouble(scan.nextLine());
            String t = scan.nextLine();

            System.out.println(i + j);

            System.out.println(d + e);

            System.out.println(s + t);

            //Scanner input = new Scanner(System.in);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
