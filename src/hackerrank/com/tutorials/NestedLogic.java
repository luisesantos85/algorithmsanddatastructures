package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

public class NestedLogic {

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int rd = in.nextInt();
            int rm = in.nextInt();
            int ry = in.nextInt();

            int ed = in.nextInt();
            int em = in.nextInt();
            int ey = in.nextInt();

            if(ry > ey){
                System.out.println(10000);
            }else if(ry < ey){
                System.out.println(0);
            }else if(rm > em){
                int auxm = rm - em;
                System.out.println(500 * auxm);
            }else if(rd > ed){
                int auxd = rd - ed;
                System.out.println(15 * auxd);
            }else{
                System.out.println(0);
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
