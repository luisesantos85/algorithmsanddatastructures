package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

/**
 * Created by luisesantos on 11/20/16.
 */
public class LetsReview {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try {
            File file = new File("/Users/luisesantos/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);

            int T = Integer.parseInt(input.nextLine());

            for(int i=0;i<T;i++){
                char[] s = input.nextLine().toCharArray();
                String even = "";
                String odd = "";

                for(int j=0;j<s.length;j++){

                    if(j%2==0){
                        even = even + s[j];
                    }else{
                        odd = odd + s[j];
                    }
                }
                System.out.println(even + " " +odd);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
