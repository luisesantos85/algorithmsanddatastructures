package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

public class BitwiseOperator {

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            // Scanner in = new Scanner(System.in);
            int t = in.nextInt();
            for(int a0 = 0; a0 < t; a0++){
                int n = in.nextInt();
                int k = in.nextInt();
                if(((k-1)|k) > n && k%2==0){
                    System.out.println(k-2);
                }else{
                    System.out.println(k-1);
                }
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
/**
 *  Objective
 *  Welcome to the last day! Today, we're discussing bitwise operations. Check out the Tutorial tab for
 *  learning materials and an instructional video!
 *
 *  Task
 *  Given set S={1,2,3...,N}. Find two integers, A and B (where a<B), from set S such that the value of A&B is the maximum
 *  possible and also less than a given integer, K. In this case, & represents the bitwise AND operator.
 *
 *  Input Format
 *  The first line contains an integer, T, the number of test cases.
 *  Each of the T subsequent lines defines a test case as 2 space-separated integers, N and K, respectively.
 *
 *  Output Format
 *  For each test case, print the maximum possible value of A&B on a new line.
 *
 *  Sample Input
 *  3
 *  5 2
 *  8 5
 *  2 2
 *
 *  Sample Output
 *  1
 *  4
 *  0
 * */