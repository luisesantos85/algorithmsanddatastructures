package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

/**
 *
 */
public class MoreLinkedLists {

    public static Node removeDuplicates(Node head) {
        //Write your code here
        if(head == null)
            return null;

        if(head.next == null)
            return head;

        if(head.data == head.next.data){
            if(head.next.next == null){
                head.next = null;
            }else{
                head.next = head.next.next;
                removeDuplicates(head);
            }
        }else{
            removeDuplicates(head.next);
        }

        return head;
    }

    public static  Node insert(Node head,int data)
    {
        Node p=new Node(data);
        if(head==null)
            head=p;
        else if(head.next==null)
            head.next=p;
        else
        {
            Node start=head;
            while(start.next!=null)
                start=start.next;
            start.next=p;

        }
        return head;
    }
    public static void display(Node head)
    {
        Node start=head;
        while(start!=null)
        {
            System.out.print(start.data+" ");
            start=start.next;
        }
    }
    public static void main(String args[])
    {
        try {

            File file = new File("/Users/luis/dev/input.txt");
            Scanner sc = new Scanner(file);

            //Scanner sc=new Scanner(System.in);
            Node head=null;
            int T=sc.nextInt();
            while(T-->0){
                int ele=sc.nextInt();
                head=insert(head,ele);
            }
            head=removeDuplicates(head);
            display(head);

        }catch(Exception e) {
            e.printStackTrace();
        }
    }
}
