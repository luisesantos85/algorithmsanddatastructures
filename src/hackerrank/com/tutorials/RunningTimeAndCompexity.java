package hackerrank.com.tutorials;

import java.io.File;
import java.util.Scanner;

public class RunningTimeAndCompexity {
    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int T = in.nextInt();

            for(int i=0;i<T;i++){
                int number = in.nextInt();

                if(number == 1){
                    System.out.println("Not prime");
                    continue;
                }

                if(number == 2){
                    System.out.println("Prime");
                    continue;
                }

                int counter=0;
                int square = (int)Math.sqrt(number);
                for(int j=1;j<=square;j++){
                    if(number%j==0)
                        counter++;
                    if(counter ==2)
                        break;
                }
                if(counter==2)
                    System.out.println("Not prime");
                else
                    System.out.println("Prime");
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
