package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class CaesarCipher {
	
	public static void main(String[] args){
		CaesarCipher.readInput();
	}
	
	public static void readInput(){
		String alphabetL = "abcdefghijklmnopqrstuvwxyz";
		String alphabetU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int N = Integer.parseInt(input.nextLine());
			if(N < 1 || N > 100){
				input.close();
				throw new IllegalArgumentException();
			}
			
			char[] s = input.nextLine().toCharArray();
			
			int K = Integer.parseInt(input.nextLine());
			if(K < 0 || K > 100){
				input.close();
				throw new IllegalArgumentException();
			}
			char[] result = new char[s.length];

			for(int i=0;i< s.length ; i++){
					String aux = "";
					int index = 0;
					if(alphabetL.indexOf(s[i]) != -1){
						aux = alphabetL;
						index = alphabetL.indexOf(s[i]);
					}else if(alphabetU.indexOf(s[i]) != -1){
						aux = alphabetU;
						index = alphabetU.indexOf(s[i]);
					}else{
						result[i] = s[i];
						continue;
					}
						
					index = K + index;
					if(index < aux.length()){
						result[i] = aux.charAt(index);
					}else{
						while(index >= aux.length()){
							index = index - aux.length();
						}
						result[i] = aux.charAt(index);
					}
			}
			System.out.println(new String(result));
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
