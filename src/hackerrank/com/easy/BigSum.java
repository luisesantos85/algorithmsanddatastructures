package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class BigSum {

	public static void main(String[] args){
		BigSum.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int N = Integer.parseInt(input.nextLine());
			
			if(N < 1 || N > 10){
				input.close();
				throw new IllegalArgumentException();
			}
			
			String[] numbers = input.nextLine().split(" ");
			long sum = 0;
			for(int i = 0; i< numbers.length; i++){
				long value = Long.parseLong(numbers[i]);
				sum += value;
			}
			System.out.println(sum);
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
