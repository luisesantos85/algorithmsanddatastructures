package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class AngryProfessor {

	public static void main(String[] args){
		AngryProfessor.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			
			if(T < 1 || T > 10){
				input.close();
				throw new IllegalArgumentException();
			}
			
			while(input.hasNextLine()){
				String[] fl = input.nextLine().split(" ");
				String[] sl = input.nextLine().split(" ");
				
				int N = Integer.parseInt(fl[0]);
				int K = Integer.parseInt(fl[1]);
				int counter =0;
				
				if(N < 1 || N > 1000 || K < 1 || N < K){
					input.close();
					throw new IllegalArgumentException();
				}
				
				for(int i = 0; i < sl.length; i++){
					int value = Integer.parseInt(sl[i]);
					if(value < -100 || value > 100){
						input.close();
						throw new IllegalArgumentException();
					}
					if(value <= 0)
						counter++;
				}
				
				if(counter < K){
					System.out.println("YES");
				}else{
					System.out.println("NO");
				}
				
			}
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
