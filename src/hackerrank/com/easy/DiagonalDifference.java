package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class DiagonalDifference {

	public static void main(String[] args){
		DiagonalDifference.readInput();
	}
	
	public static void readInput(){
		try {
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);
            int N = Integer.parseInt(input.nextLine());
            if(N < 1 || N > 100){
            	input.close();
        		throw new IllegalArgumentException();
            }
            
            int d1 = 0;
            int d2 = N-1;
            int sumD1 = 0;
            int sumD2 = 0;
            
            while(input.hasNextLine()){
            	String line = input.nextLine();
            	String[] row = line.split(" ", N);
            	sumD1 += Integer.parseInt(row[d1]);
            	d1++;
            	sumD2 += Integer.parseInt(row[d2]);
            	d2--;
            }
            int result = sumD1 - sumD2;
            System.out.println(Math.abs(result));
            
            input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
 	}	
}
