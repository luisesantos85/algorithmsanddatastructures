package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class UtopianTree {
	
	public static void main(String args[] ) throws Exception {
		UtopianTree.readInput();
	}
	
	
	public static void readInput(){
		try {
            
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);
            int T = Integer.parseInt(input.nextLine());
        	if(T < 0 || T > 10){
        		input.close();
        		throw new IllegalArgumentException();
        	}
        	
            while (input.hasNextLine()) {
                int N = Integer.parseInt(input.nextLine());
                if(N < 0 || N > 60){
            		input.close();
            		throw new IllegalArgumentException();
            	}
                
                boolean isSpring = true;
    			int height = 1;
                
    			for(int j=0; j<N;j++){
    				if(isSpring){
    					height = height*2;
    					isSpring = false;
    				}else{
    					height = height+1;
    					isSpring = true;
    				}
    			}
    			System.out.println(height);
            }
            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
 	}
}
