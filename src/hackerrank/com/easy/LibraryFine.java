package hackerrank.com.easy;

import java.io.File;
import java.util.Calendar;
import java.util.Scanner;

public class LibraryFine {
	
	public static void main(String[] args){
		LibraryFine.readInput();
	}

	public static void readInput(){
		try {
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);
            String[] returnDate = input.nextLine().split(" ");
            String[] expectedDate = input.nextLine().split(" ");
            
            
            Calendar rd = Calendar.getInstance();
            Calendar ed = Calendar.getInstance();
            rd.set(Integer.parseInt(returnDate[2]), Integer.parseInt(returnDate[1]) - 1, Integer.parseInt(returnDate[0]));
            ed.set(Integer.parseInt(expectedDate[2]), Integer.parseInt(expectedDate[1]) - 1, Integer.parseInt(expectedDate[0]));
            
            
            int result = 0;
            if(rd.getTimeInMillis() > ed.getTimeInMillis()){
            	if(rd.get(Calendar.YEAR) == ed.get(Calendar.YEAR)){
            		if(rd.get(Calendar.MONTH) == ed.get(Calendar.MONTH)){
            			if(rd.get(Calendar.DAY_OF_MONTH) == ed.get(Calendar.DAY_OF_MONTH)){
            				result = 0;
            			}else if(rd.get(Calendar.DAY_OF_MONTH) < ed.get(Calendar.DAY_OF_MONTH)){
            				result = 0;
            			}else{
            				result = 15 * (Math.abs(rd.get(Calendar.DAY_OF_MONTH) - ed.get(Calendar.DAY_OF_MONTH)));
            			}
            			
            		}else if(rd.get(Calendar.MONTH) < ed.get(Calendar.MONTH)){
            			result = 0;
            		}else{
            			result = 500 * (Math.abs(rd.get(Calendar.MONTH) - ed.get(Calendar.MONTH)));
            		}
            		
            	}else if(rd.get(Calendar.YEAR) < ed.get(Calendar.YEAR)){
            		result = 0;
            	}
            	else{
            		result = 10000;
            	}
            }
            
            System.out.println(result);
            input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
 	}	

}
