package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class Staircase {

	public static void main(String[] args){
		Staircase.readInput();
	}
	
	public static void readInput(){
		try {
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);
            int N = Integer.parseInt(input.nextLine());
            if(N < 1 || N > 100){
            	input.close();
        		throw new IllegalArgumentException();
            }
            
           for(int i = 1; i <= N; i++){
        	   for(int j = N; j >= 1; j--){
        		   if(j > i)
        			   System.out.print(" ");
        		   else
        			   System.out.print("#");
               }
        	   System.out.println("");
           }
            
            input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
 	}	
}
