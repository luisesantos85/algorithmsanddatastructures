package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class SimpleArraySum {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimpleArraySum.readInput();
	}

	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			input.nextInt();
			int sum = 0;
			
			while(input.hasNext()){
				sum += input.nextInt();
			}
			
			System.out.println(sum);
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
