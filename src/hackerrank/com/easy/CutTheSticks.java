package hackerrank.com.easy;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class CutTheSticks {

	public static void main(String[] args){
		CutTheSticks.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int N = Integer.parseInt(input.nextLine());
			
			if(N < 1 || N > 1000){
				input.close();
				throw new IllegalArgumentException();
			}
			String[] sticks = input.nextLine().split(" ");
			
			while(sticks.length > 0){
				System.out.println(sticks.length);
				int lowest = CutTheSticks.getLowest(sticks);
				sticks = CutTheSticks.getArray(sticks, lowest);
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static int getLowest(String[] array){
		int result = Integer.MAX_VALUE;
		for(int i=0;i<array.length;i++){
			int value = Integer.parseInt(array[i]);
			if(value < result)
				result = value;
			if(result == 1)
				return result;
		}
		return result;
	}
	
	public static String[] getArray(String[] array, int lowest){
		ArrayList<String> result = new ArrayList<String>();
		for(int i=0;i<array.length;i++){
			array[i] = "" + (Integer.parseInt(array[i]) - lowest);
			if(Integer.parseInt(array[i]) > 0)
				result.add(array[i]);
		}
		String[] r = new String[result.size()];
		for(int i=0;i<result.size();i++){
			String s = (String)result.get(i);
			r[i] = s;
		}
		return r;
	}
}
