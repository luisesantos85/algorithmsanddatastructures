package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class ServiceLane {

	public static void main(String args[] ) throws Exception {
		ServiceLane.readInput();
	}
	
	public static void readInput(){
		try {
            
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);
            
            String[] flArray = input.nextLine().split(" ");
            if(flArray.length != 2){
            	input.close();
        		throw new IllegalArgumentException();
            }
            
            int N = Integer.parseInt(flArray[0]);
            int T = Integer.parseInt(flArray[1]);
            if(N < 2 || N > 100000 || T < 1 || T > 1000){
            	input.close();
        		throw new IllegalArgumentException();
            }
        	
            String[] widths = input.nextLine().split(" ");
            
            
            while (input.hasNextLine()) {
            	String[] indexes = input.nextLine().split(" ");
            	int i = Integer.parseInt(indexes[0]);
            	int j = Integer.parseInt(indexes[1]);
            	int min = 4;
                for(int k = i;k<=j;k++){
                	int index = Integer.parseInt(widths[k]);
                	if(index < min)
                		min = index;
                }
    			System.out.println(min);
            }
            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
 	}
}
