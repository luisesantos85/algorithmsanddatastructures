package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class CircularArrayRotation {

	public static void main(String[] args) {
		CircularArrayRotation.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			String[] line= input.nextLine().split(" ");
			
			int k = Integer.parseInt(line[1]);
			int q = Integer.parseInt(line[2]);
			
			String[] array= input.nextLine().split(" ");
			array = CircularArrayRotation.rotate(array, k);
			
			for(int j=0;j<q;j++){
				int m = input.nextInt();
				System.out.println(array[m]);
			}
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static String[] rotate(String[] array, int k){
		String[] result = new String[array.length];
		int counter = 0;
		
		if(k > array.length)
			k = k%array.length;
		
		for(int i = array.length - k;counter<array.length;i++){
			if(i > array.length - 1)
				i = 0;
			
			result[counter]=array[i];
			counter++;
		}
		
		return result;
	}
}
