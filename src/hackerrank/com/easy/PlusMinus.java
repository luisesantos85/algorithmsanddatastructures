package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class PlusMinus {

	public static void main(String[] args){
		PlusMinus.readInput();
	}
	
	public static void readInput(){
		try {
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);
           
            int N = Integer.parseInt(input.nextLine());
            if(N < 0 || N > 100){
            	input.close();
            	throw new IllegalArgumentException();
            }
            
            String[] row;
            row = input.nextLine().split(" ", N);
            int pc = 0;
            int nc = 0;
            int cc = 0;
            
            for(int i=0; i<N;i++){
            	int value = Integer.parseInt(row[i]);
            	if(value > 0)
            		pc++;
            	if(value == 0)
            		cc++;
            	if(value < 0)
            		nc++;
            }
            float v1 = (float)pc / N;
            float v2 = (float)nc / N;
            float v3 = (float)cc / N;
            
            System.out.printf("%.3f", v1);
            System.out.println();
            System.out.printf("%.3f", v2);
            System.out.println();
            System.out.printf("%.3f", v3);
            System.out.println();
            
            input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
 	}	
	
}
