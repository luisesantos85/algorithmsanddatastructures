package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;


public class MaximizingXor {
	public static void main(String[] args) {
        readInput();
    }
	
	public static void readInput(){
		try {
            
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);

            while (input.hasNextLine()) {
                String line = input.nextLine();
                System.out.println(line);
            }
            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
 	}

}
