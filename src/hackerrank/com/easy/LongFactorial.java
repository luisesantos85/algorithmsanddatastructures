package hackerrank.com.easy;

import java.io.File;
import java.math.BigInteger;
import java.util.Scanner;

public class LongFactorial {

	public static void main(String[] args){
		LongFactorial.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int N = Integer.parseInt(input.nextLine());
			
			if(N < 1 || N > 100){
				input.close();
				throw new IllegalArgumentException();
			}
			BigInteger fact = new BigInteger("1");
			for(int i = 1; i<= N; i++){
				fact = fact.multiply(new BigInteger(Integer.toString(i)));
			}
			System.out.println(fact.toString());
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
