package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class HalloweenParty {

	public static void main(String[] args){
		HalloweenParty.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			int T = Integer.parseInt(input.nextLine());
			
			if(T < 1 || T > 10){
				input.close();
				throw new IllegalArgumentException();
			}
			
			long[] pieces = new long[10000000];
			int alpha = 1;
			pieces[2] = 1;
			pieces[3] = 2;
			for(int i=4;i<10000000;i++){
				if(i%2 == 0){
					alpha++;
				}
				pieces[i] = pieces[i-1] + alpha;
			}
			
			while(input.hasNextLine()){
				int K = Integer.parseInt(input.nextLine());
				if(K < 2 || K > 10000000){
					input.close();
					throw new IllegalArgumentException();
				}
				System.out.println(pieces[K]);
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
