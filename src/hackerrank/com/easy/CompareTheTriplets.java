package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class CompareTheTriplets {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CompareTheTriplets.readInput();
	}

	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			String[] alice = input.nextLine().split(" ");
			String[] bob = input.nextLine().split(" ");
			
			int alicePts = 0;
			int bobPts = 0;
			
			if(Integer.parseInt(alice[0]) > Integer.parseInt(bob[0])){
				alicePts++;
			}else if(Integer.parseInt(alice[0]) < Integer.parseInt(bob[0])){
				bobPts++;
			}
			
			if(Integer.parseInt(alice[1]) > Integer.parseInt(bob[1])){
				alicePts++;
			}else if(Integer.parseInt(alice[1]) < Integer.parseInt(bob[1])){
				bobPts++;
			}
			
			if(Integer.parseInt(alice[2]) > Integer.parseInt(bob[2])){
				alicePts++;
			}else if(Integer.parseInt(alice[2]) < Integer.parseInt(bob[2])){
				bobPts++;
			}
			
			System.out.println(alicePts + " " + bobPts);
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
