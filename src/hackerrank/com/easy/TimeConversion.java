package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class TimeConversion {

	public static void main(String[] args){
		TimeConversion.readInput();
	}
	
	public static void readInput(){
		try {
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);
            String[] T = input.nextLine().split(":");
            String hh = T[0];
            String mm = T[1];
            String aux = T[2];
            String ss = aux.substring(0, 2);
            String t = aux.substring(2, 4);
            
            if(t.equals("PM")){
            	if(!hh.equals("12")){
            		int v = Integer.parseInt(hh) + 12; 
                	hh = v + "";
            	}
            }else{
            	if(hh.equals("12")){
            		hh = "00";
            	}
            }
            
            System.out.println(hh +":"+ mm +":"+ ss);
            	 
            input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
 	}	
}
