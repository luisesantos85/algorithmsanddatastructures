package hackerrank.com.easy;

import java.io.File;
import java.util.Scanner;

public class LoveLetterMistery {

	public static void main(String args[] ) throws Exception {
		LoveLetterMistery.readInput();
	}
	
	
	public static void readInput(){
		try {
            
			String alphabet = "abcdefghijklmnopqrstuvwxyz";
			
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);
            int T = Integer.parseInt(input.nextLine());
        	if(T < 0 || T > 10){
        		input.close();
        		throw new IllegalArgumentException();
        	}
        	int result = 0;
            while (input.hasNextLine()) {
            	result = 0;
                StringBuilder word = new StringBuilder(input.nextLine());
                int length = word.length();
                
                if(length < 1 || length > Math.pow(10, 4)){
            		input.close();
            		throw new IllegalArgumentException();
            	}
                int middle = 0;
                if(length % 2==0){
                	middle = length / 2;
                }else{
                	Double d = Math.floor(length / 2);
                	middle = d.intValue();
                }
                
                for(int i = 0; i < middle; i++){
            		char c1 = word.charAt(i);
            		char c2 = word.charAt(length - 1 - i);
            		int cIndex1 = alphabet.indexOf(c1);
            		int cIndex2 = alphabet.indexOf(c2);
            		int aux = cIndex1 - cIndex2;
            		result += (aux<0)?aux*(-1):aux;
            	}
                
                System.out.println(result);
            }
            input.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
 	}
}
