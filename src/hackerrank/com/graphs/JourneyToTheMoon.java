package hackerrank.com.graphs;

import java.io.File;
import java.math.BigInteger;
import java.util.*;

public class JourneyToTheMoon {

    static ArrayList<Integer> nodes = new ArrayList<Integer>();

    public static void main(String[] args) {
        JourneyToTheMoon s = new JourneyToTheMoon();
        s.readInput();
    }

    public void readInput() {
        try {
            File file = new File("/Users/luis/dev/input.txt");
            Scanner input = new Scanner(file);

            //Scanner input = new Scanner(System.in);

            int N = input.nextInt();
            int I = input.nextInt();

            ArrayList<ArrayList<Integer>> adjacencyList = new ArrayList<ArrayList<Integer>>();
            boolean[] visited = new boolean[N];

            for(int j=0;j<N;j++){
                nodes.add(j);
                adjacencyList.add(new ArrayList<Integer>());
            }

            for(int i=0;i<I;i++){
                int A = input.nextInt();
                int B = input.nextInt();

                adjacencyList.get(A).add(B);
                adjacencyList.get(B).add(A);
            }

            int groupCounter = 0;
            ArrayList<Integer> map = new ArrayList<Integer>();
            ArrayList<Integer> nodesAux = new ArrayList(nodes);

            while(nodesAux.size() > 0){

                int node = nodesAux.get(0);
                ArrayList visitedArray = new ArrayList();

                if(adjacencyList.get(node).size() == 0){
                    visitedArray.add(node);
                    map.add(1);
                    groupCounter++;

                    nodesAux.remove(0);
                    continue;
                }

                //This section here is to keep record of the groups

                Stack<Integer> st = new Stack<Integer>();
                st.push(nodesAux.get(0));

                //DFS starts Here
                while(!st.isEmpty()){
                    int v = st.pop();
                    if(!visited[v]){
                        visited[v] = true;

                        visitedArray.add(v);
                        int index = nodesAux.indexOf(v);
                        nodesAux.remove(index);

                        Stack<Integer> auxStack = new Stack<Integer>();

                        for(int w : adjacencyList.get(v)){
                            if(!visited[w]){
                                auxStack.push(w);
                            }
                        }
                        while(!auxStack.isEmpty()){
                            st.push(auxStack.pop());
                        }
                    }
                }

                map.add(visitedArray.size());
                groupCounter++;
            }

            BigInteger numberOfPairs = new BigInteger("0");
            BigInteger elementsEncountered = new BigInteger("0");

            for(int k = groupCounter - 1 ; k >= 0 ; k--) {
                BigInteger sizeOfCurrentSet = BigInteger.valueOf(map.get(k));

                BigInteger numberOfNewPairs = sizeOfCurrentSet.multiply(elementsEncountered);

                numberOfPairs = numberOfPairs.add(numberOfNewPairs);
                elementsEncountered = elementsEncountered.add(sizeOfCurrentSet);
            }

            System.out.print(numberOfPairs);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
