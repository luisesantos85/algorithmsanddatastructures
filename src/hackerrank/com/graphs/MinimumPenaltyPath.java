package hackerrank.com.graphs;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class MinimumPenaltyPath {

    int N = 0;
    /**
     * TODO:
     *
     * HIS PROBLEM CAN NOT BE SOLVED USING DIJKSTRA BECAUSE DIJKSTRA IS A GREEDY ALGORITHM
     * AND IN THIS CASE NOT ALWAYS THE BEST LOCAL SOLUTION IS THE BEST GLOBAL SOLUTION.
     *
     * WE ARE COMPARING COSTS IN A BINARY WAY. I NEED TO FIND AN
     * ALGORITHM AND A DATA STRUCTURE THAT ALLOWS ME TO SAVE EVERY POSSIBLE PATH FROM
     * POINT 'A' TO POINT 'B' WITHOUT REPETITION
     *
     * */
    public static void main(String[] args){
        MinimumPenaltyPath m = new MinimumPenaltyPath();
        m.readInput();
    }

    public void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            N = in.nextInt();   //nodes
            int M = in.nextInt(); //edges
            int i, U, V, cost;

            ArrayList<Edge> adj[] = new ArrayList[N];

            int[] distances = new int[N];
            boolean[] visited = new boolean[N];  // I can get away with this since the nodes are numbers

            for(i = 0;i<N;i++){
                adj[i] = new ArrayList<Edge>();
                distances[i] = Integer.MAX_VALUE;
                visited[i] = false;
            }

            for(i = 0;i<M;i++){
                U = in.nextInt() - 1;
                V = in.nextInt() - 1;
                cost = in.nextInt();

                adj[U].add(new Edge(V, cost));
                adj[V].add(new Edge(U, cost));
            }

            int A = in.nextInt() - 1;  //origin
            int B = in.nextInt() - 1;  //destination

            distances[A] = 0;

            for (int count = 0; count < N-1; count++) {
                int u = minDistance(distances, visited);

                visited[u] = true;

                for (int v = 0; v < N; v++){
                    if (!visited[v]){

                        ArrayList<Edge> adjacencyList = adj[u];
                        boolean isAdjacent = false;
                        Edge e = null;
                        Edge auxEdge;
                        int min = Integer.MAX_VALUE;

                        for(i = 0; i<adjacencyList.size();i++){
                            auxEdge = adjacencyList.get(i);
                            if(auxEdge.end == v && auxEdge.cost < min){
                                min = auxEdge.cost;
                                e = auxEdge;
                                isAdjacent = true;
                                //break;  I CAN'T BREAK BECAUSE THAT WOULD ASSUME THERE IS ONLY ONE EDGE BETWEEN TWO NODES
                            }
                        }

                        if(isAdjacent &&
                                distances[u] != Integer.MAX_VALUE &&
                                distances[u] + e.cost < distances[v]){
                            //distances[v] = distances[u] + e.cost;
                            distances[v] = e.cost; // Since in this problem we don't care about total cost we do not add the costs.
                        }
                    }
                }
            }

            System.out.println(Arrays.toString(distances));

            int result=0;
            int size = distances.length;

            if(size==1){
                System.out.println(result);
            }else{
                for(i=A;i<=B-1;i++){
                    result = distances[i] | distances[i+1];
                }
            }

            System.out.println(result);
            //System.out.println(0 | 1 | 3);
            System.out.println(2 | 2);

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    int minDistance(int dist[], boolean sptSet[]) {

        // Initialize min value
        int min = Integer.MAX_VALUE, min_index=-1;

        for (int v = 0; v < N; v++)
            if (sptSet[v] == false && dist[v] <= min) {
                min = dist[v];
                min_index = v;
            }

        return min_index;
    }

    class Edge{
        int end;
        int cost;

        Edge(int end, int cost){
            this.end = end;
            this.cost = cost;
        }
    }
}
