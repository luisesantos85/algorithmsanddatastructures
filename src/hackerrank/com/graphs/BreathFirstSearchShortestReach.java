package hackerrank.com.graphs;

import java.io.File;
import java.util.*;


public class BreathFirstSearchShortestReach {
    static int LENGTH = 6;

    public static void main(String[] args){
        readInput();
    }

    public static void readInput(){
        try{
            File file = new File("/Users/luis/dev/input.txt");
            Scanner in = new Scanner(file);

            //Scanner in = new Scanner(System.in);

            int q = in.nextInt();

            for(int i=0; i<q;i++){
                int n = in.nextInt();
                int m = in.nextInt(); //number of edges

                ArrayList<Integer>[] adj = new ArrayList[n];
                for(int l=0; l<n;l++){
                    adj[l] = new ArrayList<Integer>();
                }

                for(int j=0; j<m;j++){
                    int node1 = in.nextInt()-1;  // substract 1 since nodes start at 1
                    int node2 = in.nextInt()-1;

                    adj[node1].add(node2);
                    adj[node2].add(node1);
                }

                int s = in.nextInt()-1; //Starting node

                //BFS starts here
                boolean[] visited = new boolean[n];
                int[] distances = new int[n];
                Arrays.fill(distances, -1);

                LinkedList<Integer> queue = new LinkedList<Integer>();

                visited[s]=true; // Mark the current node as visited and enqueue it
                queue.add(s);

                distances[s] = 0;

                while (queue.size() != 0){
                    s = queue.poll(); // Dequeue a vertex from queue and print it
                    Iterator<Integer> it = adj[s].listIterator();

                    while (it.hasNext()){

                        int neighbor = it.next();
                        if (!visited[neighbor]){
                            distances[neighbor] = distances[s] + LENGTH;
                            visited[neighbor] = true;
                            queue.add(neighbor);
                        }
                    }
                }

                for(int x = 0;x<distances.length;x++){
                    if(distances[x] != 0){
                        System.out.print(distances[x] + " ");
                    }
                }
                System.out.println();
            }

            in.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
