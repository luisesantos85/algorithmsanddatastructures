package hackerrank.com.graphs;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class PrimSpecialSubtree {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PrimSpecialSubtree.readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
						
			int nodes = input.nextInt();
			int edges = input.nextInt();
			
			int[][] graph = new int[nodes][nodes];
			int i=0;
			
			for(i=0;i<nodes;i++){
				Arrays.fill(graph[i], Integer.MAX_VALUE);
			}
			
			for(i=0;i<edges;i++){
				int node1 = input.nextInt();
				int node2 = input.nextInt();
				int edge = input.nextInt();
				
				graph[node1-1][node2-1] = edge;
				graph[node2-1][node1-1] = edge;
			}
			
			int s = input.nextInt() - 1;  //first node to reach
			
			boolean[] reached = new boolean[nodes];	// Reach or unreach nodes
			int[] predNode = new int[nodes];		// Remember min cost edge

			/* 
		     	ReachSet = {0}; 
			 	UnReachSet = {1, 2, ..., N-1};
			*/
			for ( i = 0; i < nodes; i++ ){
				reached[i] = false;
			}	
			reached[s] = true;

			
			
			predNode[s] = 0; 
			
			int x,y,k,j=0;
			int sum = 0;
			for ( k = 1; k < nodes; k++){  
				x = y = 0;

					for ( i = 0; i < nodes; i++ ){
			            for ( j = 0; j < nodes; j++ ){
			            	if ( reached[i] && ! reached[j] && graph[i][j] < graph[x][y] ){
			                   x = i;     // Link (i,j) has lower cost !!!
			                   y = j;
			            	}
			            }
					}

			        predNode[y] = x;
			        reached[y] = true;
			        sum = sum + graph[x][y];
			}
			System.out.println(sum);
			input.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void printGraph(int[][] graph){
		for(int i=0;i<graph.length;i++){
			System.out.println(Arrays.toString(graph[i]));
		}
	}
}
