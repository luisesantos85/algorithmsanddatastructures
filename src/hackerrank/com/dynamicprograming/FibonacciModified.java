package hackerrank.com.dynamicprograming;

import java.io.File;
import java.math.BigInteger;
import java.util.Scanner;

public class FibonacciModified {

	public static BigInteger[] fib; 
	public static BigInteger t1;
	public static BigInteger t2;
	public static int n;
	
	public static void main(String[] args) {
		readInput();
		fib = new BigInteger[n+1];
		fib[0] = BigInteger.valueOf(0);
		fib[1] = t1;
		fib[2] = t2;
		System.out.println(fibModified(n));
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			t1 = input.nextBigInteger();
			t2 = input.nextBigInteger();
			n = input.nextInt();
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static BigInteger fibModified(int n){
		if (n <= 2){ 
			return fib[n];
		}else{
			return fibModified(n-2).add(fibModified(n-1).pow(2));
		}
	}
}
