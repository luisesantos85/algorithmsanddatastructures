package hackerrank.com.dynamicprograming;

import java.io.File;
import java.util.Scanner;

public class TheMaximumSubarray {
	
	public static int T;
	public static int[] totals;
	public static String[] array;
	public static int maximumC= Integer.MIN_VALUE;
	public static int maximumN= Integer.MIN_VALUE;
	
	public static void main(String[] args) {
		readInput();
	}
	
	public static void readInput(){
		try{
			File file = new File("/Users/luis/dev/input.txt");
			Scanner input = new Scanner(file);
			
			//Scanner input = new Scanner(System.in);
			
			T = Integer.parseInt(input.nextLine());
			int N = 0;
			
			for(int i = 0; i < T; i++){
				maximumC = 0;
				maximumN = 0;
				
				N = Integer.parseInt(input.nextLine());
				array = input.nextLine().split(" ");
				
				totals = new int [N];
				totals[0] = Integer.parseInt(array[0]);
				
				calculateContiguos();
				System.out.println(maximumC + " " + maximumN);
			}
			
			input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void calculateContiguos(){
		maximumC = Integer.parseInt(array[0]);
		int maximumNonContiguous = 0;
		int maximumNonContiguous2 = Integer.MIN_VALUE;
		
		boolean foundPositive = false;

		//Doing it with loops may end up in a worst performance than doing it with recursion or dynamic programming
		for(int i=1; i <= array.length-1; i ++){
			/***Contiguous***/
			totals[i] = Integer.parseInt(array[i]) + totals[i-1];
			if(totals[i] > maximumC)
				maximumC = totals[i];
			
			if(Integer.parseInt(array[i]) > maximumC){
				maximumC = Integer.parseInt(array[i]);
			}
			
			if(Integer.parseInt(array[i]) > 0){
				maximumNonContiguous+= Integer.parseInt(array[i]);
				foundPositive = true;
			}
			
			if(!foundPositive){
				if(Integer.parseInt(array[i]) > maximumNonContiguous2){
					maximumNonContiguous2 = Integer.parseInt(array[i]);
				}
			}
		}
		
		if(foundPositive){
			if(Integer.parseInt(array[0]) > 0){
				maximumNonContiguous += Integer.parseInt(array[0]);
			}
		}else{
			if(Integer.parseInt(array[0]) > maximumNonContiguous2){
				maximumNonContiguous2 = Integer.parseInt(array[0]);
			}
		}
		
		maximumN = (foundPositive)?maximumNonContiguous:maximumNonContiguous2;
	} 
}
