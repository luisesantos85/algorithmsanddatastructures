package main.collections;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//JAVA Collection diagram:
// https://docs.oracle.com/javase/tutorial/collections/interfaces/index.html

public class Collections {

	//There are two letters e intentionally to show that the sets don't allow repeated items
	public static String[] letters = {"a", "b", "c", "d", "e", "e"}; 
	
	public static void main(String[] args){
		Collections.listExample();
		Collections.setExample();
		Collections.mapExample();
	}
	
	/**
	 * Array with dynamic size 
	 * */
	public static void listExample(){
		List<String> list = Arrays.asList(letters); //Creates a list from an Array
		System.out.println("LIST:");
		System.out.println(list);
		System.out.println();
	}
	
	/**
	 * Collection that CAN NOT have duplicated elements
	 * */
	public static void setExample(){
		Set<String> set = new HashSet<String>(Arrays.asList(letters));
		System.out.println("SET (Doesnt accept repeated items):");
		System.out.println(set);
		System.out.println();
	}
	
	/**
	 * Key Value pairs.
	 * */
	public static void mapExample(){
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for (int i= 0; i < letters.length ; i++)
			  map.put(i, letters[i]);
		
		System.out.println("MAP:");
		System.out.println(map);
		System.out.println();
		
		//Iteration through a map
		/*for (Map.Entry<Integer, String> entry : map.entrySet()) {
			Integer key = entry.getKey();
		    String value = entry.getValue();
		}*/
		
		/*
		 * You can have HashMap or HashTable.
		 * HashTable is synchronized which means only one thread can modify the 
		 * hash at one point in time.
		 * HashMap is not synchronized. Use this unless you are working with 
		 * multi threaded applications 
		 * */
	}
}
