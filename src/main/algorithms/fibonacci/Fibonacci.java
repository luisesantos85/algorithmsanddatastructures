package main.algorithms.fibonacci;

/**
 * By definition, the first two numbers in the Fibonacci sequence 
 * are either 1 and 1, or 0 and 1, depending on the chosen starting 
 * point of the sequence, and each subsequent number 
 * is the sum of the previous two.
 * */
public class Fibonacci {

	public static void main(String args[]){    
		int n1=0,n2=1,n3,i,count=10;    
		System.out.print(n1+" "+n2);//printing 0 and 1    
	    
		for(i=2;i<count;++i){//loop starts from 2 because 0 and 1 are already printed        
			n3=n1+n2;    
			System.out.print(" "+n3);    
			n1=n2;    
			n2=n3;    
		}    
	}
}
