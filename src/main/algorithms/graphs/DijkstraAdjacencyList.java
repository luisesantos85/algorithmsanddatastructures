package main.algorithms.graphs;

import java.util.ArrayList;


/**
 * ALGORITHM FOR UNDIRECTED GRAPH
 */
public class DijkstraAdjacencyList {

    // from the set of vertices not yet included in shortest path tree
    static final int V=9;
    int minDistance(int dist[], Boolean sptSet[]) {

        // Initialize min value
        int min = Integer.MAX_VALUE, min_index=-1;

        for (int v = 0; v < V; v++)
            if (sptSet[v] == false && dist[v] <= min) {
                min = dist[v];
                min_index = v;
            }

        return min_index;
    }

    // A utility function to print the constructed distance array
    void printSolution(int dist[], int n) {

        System.out.println("Vertex   Distance from Source");
        for (int i = 0; i < V; i++)
            System.out.println(i+" \t\t "+dist[i]);

    }

    // Function that implements Dijkstra's single source shortest path
    // algorithm for a graph represented using adjacency list
    // representation
    void dijkstra(ArrayList<Edge> graph[], int src) {

        int dist[] = new int[V]; // The output array. dist[i] will hold
        // the shortest distance from src to i

        // sptSet[i] will true if vertex i is included in shortest
        // path tree or shortest distance from src to i is finalized
        Boolean sptSet[] = new Boolean[V];
        int i = 0;
        // Initialize all distances as INFINITE and stpSet[] as false
        for (i = 0; i < V; i++) {
            dist[i] = Integer.MAX_VALUE;
            sptSet[i] = false;
        }

        // Distance of source vertex from itself is always 0
        dist[src] = 0;

        // Find shortest path for all vertices
        for (int count = 0; count < V-1; count++) {

            // Pick the minimum distance vertex from the set of vertices
            // not yet processed. u is always equal to src in first
            // iteration.
            int u = minDistance(dist, sptSet);

            // Mark the picked vertex as processed
            sptSet[u] = true;

            // Update dist value of the adjacent vertices of the
            // picked vertex.
            for (int v = 0; v < V; v++)

                // Update dist[v] only if is not in sptSet, there is an
                // edge from u to v, and total weight of path from src to
                // v through u is smaller than current value of dist[v]
                if (!sptSet[v]){
                    ArrayList<Edge> adjacencyList = graph[u];
                    boolean isAdjacent = false;
                    Edge e = null;
                    Edge auxEdge;
                    int min = Integer.MAX_VALUE;

                    for(i = 0; i<adjacencyList.size();i++){
                         auxEdge = adjacencyList.get(i);
                        if(auxEdge.end == v && auxEdge.cost < min){
                            min = auxEdge.cost;
                            e = auxEdge;
                            isAdjacent = true;
                            //break;  I CAN'T BREAK BECAUSE THAT WOULD ASSUME THERE IS ONLY ONE EDGE BETWEEN TWO NODES
                        }
                    }

                    if(isAdjacent &&
                            dist[u] != Integer.MAX_VALUE &&
                            dist[u] + e.cost < dist[v]){
                        dist[v] = dist[u] + e.cost;
                    }
                }
        }

        // print the constructed distance array
        printSolution(dist, V);
    }

    public static void main (String[] args) {

        ArrayList<Edge> graph[] = new ArrayList[V];
        for(int i=0; i < V; i++){
            graph[i] = new ArrayList<Edge>();
        }
        graph[0].add(new Edge(1, 4));
        graph[0].add(new Edge(7, 8));

        graph[1].add(new Edge(0, 4));
        graph[1].add(new Edge(7, 11));
        graph[1].add(new Edge(2, 8));

        graph[2].add(new Edge(1, 8));
        graph[2].add(new Edge(8, 2));
        graph[2].add(new Edge(3, 7));
        graph[2].add(new Edge(5, 4));

        graph[3].add(new Edge(2, 7));
        graph[3].add(new Edge(5, 14));
        graph[3].add(new Edge(4, 9));

        graph[4].add(new Edge(3, 9));
        graph[4].add(new Edge(5, 10));

        graph[5].add(new Edge(4, 10));
        graph[5].add(new Edge(3, 14));
        graph[5].add(new Edge(2, 4));
        graph[5].add(new Edge(6, 2));

        graph[6].add(new Edge(5, 2));
        graph[6].add(new Edge(8, 6));
        graph[6].add(new Edge(7, 1));

        graph[7].add(new Edge(6, 1));
        graph[7].add(new Edge(8, 7));
        graph[7].add(new Edge(1, 11));
        graph[7].add(new Edge(0, 8));

        graph[8].add(new Edge(2, 2));
        graph[8].add(new Edge(6, 6));
        graph[8].add(new Edge(7, 7));


        DijkstraAdjacencyList t = new DijkstraAdjacencyList();
        t.dijkstra(graph, 0); // Undirected Graph
    }

    static class Edge{
        int end;
        int cost;

        Edge(int end, int cost){
            this.end = end;
            this.cost = cost;
        }
    }
}

/**
 * http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/
 * */
