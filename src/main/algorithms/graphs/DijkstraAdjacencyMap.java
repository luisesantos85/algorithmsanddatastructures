package main.algorithms.graphs;

import java.util.HashMap;
import java.util.Map;

/**
 * DOES NOT ALLOW MULTIPLE EDGES BETWEEN NODES. ALGORITHM FOR UNDIRECTED GRAPH
 */
public class DijkstraAdjacencyMap {

    // from the set of vertices not yet included in shortest path tree
    static final int V=9;
    int minDistance(int dist[], Boolean sptSet[]) {

        // Initialize min value
        int min = Integer.MAX_VALUE, min_index=-1;

        for (int v = 0; v < V; v++)
            if (sptSet[v] == false && dist[v] <= min) {
                min = dist[v];
                min_index = v;
            }

        return min_index;
    }

    // A utility function to print the constructed distance array
    void printSolution(int dist[], int n) {

        System.out.println("Vertex   Distance from Source");
        for (int i = 0; i < V; i++)
            System.out.println(i+" \t\t "+dist[i]);

    }

    // Function that implements Dijkstra's single source shortest path
    // algorithm for a graph represented using adjacency list
    // representation
    void dijkstra(Map<Integer, Integer> graph[], int src) {

        int dist[] = new int[V]; // The output array. dist[i] will hold
        // the shortest distance from src to i

        // sptSet[i] will true if vertex i is included in shortest
        // path tree or shortest distance from src to i is finalized
        Boolean sptSet[] = new Boolean[V];

        // Initialize all distances as INFINITE and stpSet[] as false
        for (int i = 0; i < V; i++) {
            dist[i] = Integer.MAX_VALUE;
            sptSet[i] = false;
        }

        // Distance of source vertex from itself is always 0
        dist[src] = 0;

        // Find shortest path for all vertices
        for (int count = 0; count < V-1; count++) {

            // Pick the minimum distance vertex from the set of vertices
            // not yet processed. u is always equal to src in first
            // iteration.
            int u = minDistance(dist, sptSet);

            // Mark the picked vertex as processed
            sptSet[u] = true;

            // Update dist value of the adjacent vertices of the
            // picked vertex.
            for (int v = 0; v < V; v++)

                // Update dist[v] only if is not in sptSet, there is an
                // edge from u to v, and total weight of path from src to
                // v through u is smaller than current value of dist[v]
                if(!sptSet[v] && graph[u].containsKey(v)
                        && dist[u] != Integer.MAX_VALUE
                        && dist[u]+graph[u].get(v) < dist[v])

                    dist[v] = dist[u] + graph[u].get(v);
        }

        // print the constructed distance array
        printSolution(dist, V);
    }

    public static void main (String[] args) {

        // Is better to use a map instead of a list so we can check if 2 nodes are adjacent to each other easily.
        // If we want to use a list we will need to have another class called Edge (LinkedList<Edge> graph[]) and manually check for adjacency

        //key: Adjacent node.
        //value: cost to the adjacent node
        Map<Integer, Integer> graph[] = new HashMap[V];
        for(int i=0; i<V;i++){
            graph[i] = new HashMap<Integer, Integer>();
        }
        graph[0].put(1,4);
        graph[0].put(7,8);

        graph[1].put(0,4);
        graph[1].put(7,11);
        graph[1].put(2,8);

        graph[2].put(1,8);
        graph[2].put(8,2);
        graph[2].put(3,7);
        graph[2].put(5,4);

        graph[3].put(2,7);
        graph[3].put(5,14);
        graph[3].put(4,9);

        graph[4].put(3,9);
        graph[4].put(5,10);

        graph[5].put(4,10);
        graph[5].put(3,14);
        graph[5].put(2,4);
        graph[5].put(6,2);

        graph[6].put(5,2);
        graph[6].put(8,6);
        graph[6].put(7,1);

        graph[7].put(6,1);
        graph[7].put(8,7);
        graph[7].put(1,11);
        graph[7].put(0,8);

        graph[8].put(2,2);
        graph[8].put(6,6);
        graph[8].put(7,7);

        DijkstraAdjacencyMap t = new DijkstraAdjacencyMap();
        t.dijkstra(graph, 0); // Undirected Graph
    }
}
