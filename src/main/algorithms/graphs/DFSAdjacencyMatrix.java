package main.algorithms.graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

/**
 * Adjacency Matrix vs Adjacency List:
 *
 * MEMORY (better for list that matrix)
 *
 * If memory is your primary concern you can follow this formula for a simple graph that allows loops:
 * An adjacency matrix occupies n2/8 byte space (one bit per entry).
 * An adjacency list occupies 8e space, where e is the number of edges (32bit computer).
 * If we define the density of the graph as d = e/n2 (number of edges divided by the maximum number of edges),
 * we can find the "breakpoint" where a list takes up more memory than a matrix:
 *
 * 8e > n2/8 when d > 1/64
 *
 * So with these numbers (still 32-bit specific) the breakpoint lands at 1/64. If the density (e/n2) is bigger than 1/64,
 * then a matrix is preferable if you want to save memory.
 * Side note: One can improve the space-efficiency of the adjacency matrix by using a hash table where the keys are pairs
 * of vertices (undirected only).
 *
 *
 * ITERATION & LOOKUP
 *
 * Adjacency lists are a compact way of representing only existing edges.
 * However, this comes at the cost of possibly slow lookup of specific edges. Since each list is as long as
 * the degree of a vertex the worst case lookup time of checking for a specific edge can become O(n),
 * if the list is unordered. However, looking up the neighbours of a vertex becomes trivial,
 * and for a sparse or small graph the cost of iterating through the adjacency lists might be negligible.
 *
 * Adjacency matrices on the other hand use more space in order to provide constant lookup time.
 * Since every possible entry exists you can check for the existence of an edge in constant time using indexes.
 * However, neighbour lookup takes O(n) since you need to check all possible neighbours.
 * The obvious space drawback is that for sparse graphs a lot of padding is added.
 * See the memory discussion above for more information on this.
 *
 * If you're still unsure what to use: Most real-world problems produce sparse and/or large graphs,
 * which are better suited for adjacency list representations.
 * They might seem harder to implement but I assure you they aren't,
 * and when you write a BFSAdjacencyMatrix or DFS and want to fetch all neighbours of a node they're just one line of code away.
 * However, note that I'm not promoting adjacency lists in general.
 */
public class DFSAdjacencyMatrix {

    static ArrayList<Node> nodes = new ArrayList<Node>();
    static class Node {
        int data;
        boolean visited;

        Node(int data) {
            this.data=data;
        }
    }

    public ArrayList<Node> findNeighbours(int adjacency_matrix[][],Node x) {
        int nodeIndex=-1;
        ArrayList<Node> neighbours=new ArrayList<Node>();

        for (int i = 0; i < nodes.size(); i++) {
            if(nodes.get(i).equals(x)) {
                nodeIndex=i;
                break;
            }
        }

        if(nodeIndex!=-1) {
            for (int j = 0; j < adjacency_matrix[nodeIndex].length; j++) {
                if(adjacency_matrix[nodeIndex][j]==1) {
                    neighbours.add(nodes.get(j));
                }
            }
        }

        return neighbours;
    }

    public void dfsUsingStack(int adjacency_matrix[][], Node node) {
        Stack<Node> stack=new Stack<Node>();
        stack.add(node);
        node.visited=true;

        while (!stack.isEmpty()) {
            Node element=stack.pop();
            System.out.print(element.data + "\t");
            ArrayList<Node> neighbours=findNeighbours(adjacency_matrix,element);

            for (int i = 0; i < neighbours.size(); i++) {
                Node n=neighbours.get(i);
                if(n!=null && !n.visited) {
                    stack.add(n); n.visited=true;
                }
            }
        }
    }

    // Recursive DFS
    public void dfs(int adjacency_matrix[][], Node node) {
        System.out.print(node.data + "\t");
        ArrayList<Node> neighbours=findNeighbours(adjacency_matrix,node);
        for (int i = 0; i < neighbours.size(); i++) {
            Node n=neighbours.get(i);
            if(n!=null && !n.visited) {
                dfs(adjacency_matrix,n);
                n.visited=true;
            }
        }
    }

    public static void main(String arg[]) {
        Node node40 =new Node(40);
        Node node10 =new Node(10);
        Node node20 =new Node(20);
        Node node30 =new Node(30);
        Node node60 =new Node(60);
        Node node50 =new Node(50);
        Node node70 =new Node(70);
        nodes.add(node40);
        nodes.add(node10);
        nodes.add(node20);
        nodes.add(node30);
        nodes.add(node60);
        nodes.add(node50);
        nodes.add(node70);

        int adjacency_matrix[][] = { {0,1,1,0,0,0,0}, // Node 1: 40
         {0,0,0,1,0,0,0}, // Node 2 :10
         {0,1,0,1,1,1,0}, // Node 3: 20
         {0,0,0,0,1,0,0}, // Node 4: 30
         {0,0,0,0,0,0,1}, // Node 5: 60
         {0,0,0,0,0,0,1}, // Node 6: 50
         {0,0,0,0,0,0,0}, // Node 7: 70
         };

        DFSAdjacencyMatrix dfsExample = new DFSAdjacencyMatrix(); System.out.println("The DFS traversal of the graph using stack ");
        dfsExample.dfsUsingStack(adjacency_matrix, node40);
        System.out.println();

        clearVisitedFlags();
        System.out.println("The DFS traversal of the graph using recursion ");
        dfsExample.dfs(adjacency_matrix, node40);
    }

    public static void clearVisitedFlags() {
        for (int i = 0; i < nodes.size(); i++) {
            nodes.get(i).visited=false;
        }
    }

    public static void printArray(int [][] array){
        for(int i=0;i<array.length;i++){
            int [] line = array[i];
            System.out.println(Arrays.toString(line));
        }
    }
}
