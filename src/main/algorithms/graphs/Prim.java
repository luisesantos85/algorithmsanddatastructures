package main.algorithms.graphs;

import java.util.Arrays;

/**
 * 
 * In computer science, Prim's algorithm is a greedy algorithm that finds a Minimum Spanning Tree (MST) for a weighted undirected graph. 
 * This means it finds a subset of the edges that forms a tree that includes every vertex, where the total weight of all the edges in the tree is minimized.
 * 
 * */
public class Prim {

	public static void main(String[] args){
		int size = 9;
		int i = Integer.MAX_VALUE;
		
		int[][] originalGraph = {
		         {0,3,0,2,0,0,0,0,4},
		         {3,0,0,0,0,0,0,4,0},
		         {0,0,0,6,0,1,0,2,0},
		         {2,0,6,0,1,0,0,0,0},
		         {0,0,0,1,0,0,0,0,8},
		         {0,0,1,0,0,0,8,0,0},
		         {0,0,0,0,0,8,0,0,0},
		         {0,4,2,0,0,0,0,0,0},
		         {4,0,0,0,8,0,0,0,0}
			};
		System.out.println("Original Graph");
		System.out.println();
		printGraph(originalGraph);
		System.out.println();
		
		int[][] graph = {
	         {i,3,i,2,i,i,i,i,4},
	         {3,i,i,i,i,i,i,4,i},
	         {i,i,i,6,i,1,i,2,i},
	         {2,i,6,i,1,i,i,i,i},
	         {i,i,i,1,i,i,i,i,8},
	         {i,i,1,i,i,i,8,i,i},
	         {i,i,i,i,i,8,i,i,i},
	         {i,4,2,i,i,i,i,i,i},
	         {4,i,i,i,8,i,i,i,i}
		};
		
		calculateMST(size, graph);
	}
	
	public static void calculateMST(int NNodes, int[][] linkCosts){
		int i, j, k, x, y;

		boolean[] reached = new boolean[NNodes];	// Reach or unreach nodes
		int[] predNode = new int[NNodes];		// Remember min cost edge

		/* 
	     	ReachSet = {0}; 
		 	UnReachSet = {1, 2, ..., N-1};
		*/
		
		reached[0] = true;

		for ( i = 1; i < NNodes; i++ ){
			reached[i] = false;
		}	

		predNode[0] = 0;  // This is not necessary....
		                 // Just make sure we don't have a bogus edge


	     /* 
	        UnReachSet will decreas by 1 node in each iteration
		 	There are NNodes-1 unreached nodes; so we can loop
		 	NNodes-1 times and UnReachSet will become empty !
		 */
	      
		// Loop NNodes-1 times (UnReachSet = empty)
		for ( k = 1; k < NNodes; k++){  
		 
		/* Find min. cost link between: reached node ---> unreached node */
			x = y = 0;

			for ( i = 0; i < NNodes; i++ )
	            for ( j = 0; j < NNodes; j++ ){
	            	if ( reached[i] && ! reached[j] && linkCosts[i][j] < linkCosts[x][y] ){
	                   x = i;     // Link (i,j) has lower cost !!!
	                   y = j;
	            	}
	            }

		 /* Add e (x,y) to Spanning tree */
	         predNode[y] = x;

		 /* 
		    ReachSet = ReachSet ∪ {y}
		    UnReachSet = UnReachSet - {y}
		 */
	         reached[y] = true;
	      }

			System.out.println("Edges (x,y): preNode[y]=x");
			System.out.println();
	      	printMinCostEdges( predNode );    // Print the edges
	      	
	      	System.out.println();
	      	System.out.println("MST (Minimum Spanning Tree)");
			System.out.println();
	      	printMST( predNode, linkCosts );    // Print the MSC (Min Cost Spanning Tree)
	   }

		public static void printGraph(int[][] graph){
			for(int j=0;j<graph.length;j++){
				System.out.println(Arrays.toString(graph[j]));
			}	
		}
	
		public static void printMinCostEdges(int[] nodes){
			System.out.println(Arrays.toString(nodes));
		}
		
		public static void printMST(int[] edges, int[][] graph){
			int[][] mst = {
			         {0,0,0,0,0,0,0,0,0},
			         {0,0,0,0,0,0,0,0,0},
			         {0,0,0,0,0,0,0,0,0},
			         {0,0,0,0,0,0,0,0,0},
			         {0,0,0,0,0,0,0,0,0},
			         {0,0,0,0,0,0,0,0,0},
			         {0,0,0,0,0,0,0,0,0},
			         {0,0,0,0,0,0,0,0,0},
			         {0,0,0,0,0,0,0,0,0}
				};
			for(int i=0;i<edges.length;i++){
				mst[i][edges[i]]=graph[i][edges[i]]; //Since it is a non directioned graph I have to add it in the 2 coordinates (x,y) and (y,x)
				mst[edges[i]][i]=graph[i][edges[i]];
			}
			mst[0][0] = 0;
			for(int j=0;j<mst.length;j++){
				System.out.println(Arrays.toString(mst[j]));
			}	
		}
}
