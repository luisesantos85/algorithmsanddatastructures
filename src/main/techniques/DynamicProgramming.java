package main.techniques;

import java.util.Arrays;

/**
 * What is a dynamic programming, how can it be described?:
 *  
 * A DP is an algorithmic technique which is usually based on a recurrent 
 * formula and one (or some) starting states. 
 * A sub-solution of the problem is constructed from previously found ones. 
 * DP solutions have a polynomial complexity which assures a much faster running time 
 * than other techniques like backtracking, brute-force etc.
 * 
 * 
 * Dynamic programming is a technique to solve the recursive problems in more efficient manner.
 * Many times in recursion we solve the sub-problems repeatedly.
 * In dynamic pro­gram­ming we store the solution of these sub-problems so that
 * we do not have to solve them again, this is called Memoization.
 * 
 * Dynamic programming and memoization works together.
 * So Most of the problems are solved with two components of dynamic programming (DP)
 * 
 * - Recur­sion — Solve the sub-problems recursively
 * - Mem­o­iza­tion — Store the solu­tion of these sub-problems so that we do not have to solve them again
 * 
 * */
public class DynamicProgramming {

	public static int [] fib;
	public static int parameter = 10;
	
	public static void main(String[] arg){
		//I initialize an array to save the solutions for each state
		fib = new int[parameter + 1];
		fib[0] = 0;
		fib[1] = 1;
		
		fibTopDown(parameter);
		
		System.out.println(Arrays.toString(fib));
	}
	
	/**
	 * Fibonacci sequence using Dynamic Programming.
	 * 
	 * */
		
	public static int fibTopDown(int n) {
		if(n==0) return 1;
		if(n==1) return 1;
		if(fib[n]!=0){
			return fib[n];
		}else{
			fib[n] = fibTopDown(n-1) + fibTopDown(n-2);
			return fib[n];
		}
	}
}
